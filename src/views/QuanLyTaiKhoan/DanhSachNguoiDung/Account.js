import React, { Component } from 'react';
import {
  Container, Card, CardBody, Alert, Col, Row, Button,
  Form,
  FormGroup,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from 'reactstrap';
import { api } from '../..'
import { connect } from "react-redux";
import moment from 'moment'

class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: '',
      phone: '',
      email: '',
      accountError: '',
      emailError: '',
      phoneError: '',nameError:''
    }
  }
  onChange = async (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({
      [name]: value
    });
  }
  componentDidMount() {
    this.actGetStore()
  }
  async actGetStore() {
    var id = this.props.match.params.id
    var result = await api.dataService.actFetchStoresRequest('', 10, '', { id: id }, '')
    console.log('uuu', result)
    if (result.data.data.length === 1)
      this.setState({
        fullName: result.data.data[0].fullName,
        email: result.data.data[0].email,
        phone: result.data.data[0].phone,
     
      })
  }
  validate = () => {
    let emailError = "";
    let nameError = "";
    let phoneError = "";
    var {  name, email, phone,fullName } = this.state;
    const regexp = /^0\d{9,10}$/;
  
    if (!fullName) {
      nameError = 'Tên không được để trống !'
    }
    if (!email.includes("@")) {
      emailError = 'Email không đúng định dạng !';
    }
    if (!phone.match(regexp)) {
      phoneError = 'Số điện thoại phải bắt đầu bằng 0 và gồm 10-11 chữ số !'
    }
    if (emailError || phoneError  || nameError) {
      this.setState({ emailError, phoneError,nameError});
      return false;
    }
    return true
  }
  onSubmit = async e => {
    e.preventDefault();
    e.target.reset()
    const isValid = this.validate()
    var id = this.props.match.params.id
    var { fullName, email,phone } = this.state;
    if (isValid) {
    var store = {
      id: id,
      fullName: fullName  || undefined,
      email: email || undefined,
      phone: phone || undefined,
    }
    await api.dataService.actUpdateStoreRequest(store);
    this.props.history.goBack(-1)
  }
  }
  formatCurrency(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
  }
  render() {
    var { listStores } = this.props;
    var { email, fullName, phone ,nameError,emailError,phoneError} = this.state;
    listStores = this.props.listStores.stores
    return (
      <Container>
        <Row className="justify-content-center animated fadeIn">
          <Col md="10">
            <Card className="mx-4">
              <CardBody className="p-4">
                <Alert color="warning" className="text-center">
                  Cập nhật
      </Alert>
      <Form onSubmit={this.onSubmit.bind(this)}>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '100px' }}>Email</InputGroupText>
                  </InputGroupAddon>
                  <Input type="email" id="email" name="email" value={email} onChange={this.onChange.bind(this)} />

                </InputGroup>
                {emailError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{emailError}</i> </p> : ''}
              </FormGroup>

              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '100px' }}>Số điện thoại</InputGroupText>
                  </InputGroupAddon>
                  <Input type="number" id="phone" name="phone" min='0' step='1' value={phone} onChange={this.onChange.bind(this)} />

                </InputGroup>
                {phoneError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{phoneError}</i> </p> : ''}
              </FormGroup>

              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '100px' }}>Tên đầy đủ</InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" id="fullName" name="fullName" min='0' step='1' value={fullName} onChange={this.onChange.bind(this)} />

                </InputGroup>
                {nameError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{nameError}</i> </p> : ''}
              </FormGroup>
              <FormGroup className="form-actions text-right">
                <Button type="submit" size="sm" color="primary">Cập nhật</Button>
              </FormGroup>
            </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container >
    )
  }
}
const mapStateToProps = state => {
  return {
    userReducer: state.userReducer,
    listStores: state.listStores,
    itemEditing: state.itemEditing
  }
}
export default connect(mapStateToProps)(Account);

