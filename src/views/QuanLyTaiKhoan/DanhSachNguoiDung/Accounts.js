import React, { Component } from 'react';
import {
  Button, Row, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Table, Modal,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input, Alert,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  ModalHeader
} from 'reactstrap';
import { api } from '../..'
import { connect } from "react-redux";
import { AppSwitch } from '@coreui/react'
import qs from 'query-string'
import '../../../index.css'
import 'react-select/dist/react-select.css'
import 'react-virtualized-select/styles.css'
import Select from "react-virtualized-select";

class Accounts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false, fullName: '',
      account: '',
      password: '',
      email: '',
      accountError: '',
      emailError: '',
      phoneError: '',
      phone: '',
      count: 0,
      prev: 0,
      next: 2,
      last: 0,
      first: 1,
      currentPage: 1,
      pagesNumber: [],
    };
    this.toggleSuccess = this.toggleSuccess.bind(this);
  }
  validate = () => {
    let emailError = "";
    let accountError = "";
    let nameError = "";
    let phoneError = "";
    var { account, email, phone, fullName } = this.state;
    const regexp = /^0\d{9,10}$/;
    if (!account) {
      accountError = 'Tên tài khoản không được để trống !'
    }
    if (!fullName) {
      nameError = 'Tên không được để trống !'
    }
    if (!email.includes("@")) {
      emailError = 'Email không đúng định dạng !';
    }
    if (!phone.match(regexp)) {
      phoneError = 'Số điện thoại phải bắt đầu bằng 0 và gồm 10-11 chữ số !'
    }
    if (emailError || phoneError || accountError || nameError) {
      this.setState({ emailError, phoneError, accountError, nameError });
      return false;
    }
    return true
  }
  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }
  componentDidMount() {
    this.actFetchStore()
  }
  async actFetchStore(where) {
    let urlQueryObj = qs.parse(this.props.location.search);
    let currentPage = Number(urlQueryObj.page || 1);
    if (isNaN(currentPage) || currentPage < 1) {
      currentPage = 1;
    }
    let limit = urlQueryObj.limit || 10;
    let skip = (currentPage - 1) * limit;
    var result = await api.dataService.actFetchStoresRequest(skip, limit, '', where, [{ id: 'ASC' }])
    console.log('rrr', result)
    let count = result.data.count;
    let maxPage = Math.ceil(count / limit);
    let pagesNumber = [];

    if (maxPage <= 5) {
      for (let i = 0; i < maxPage; i++) {
        pagesNumber.push(i + 1);
      }

    }
    else if (maxPage > 5 && maxPage - 2 <= currentPage) {
      pagesNumber = [0, maxPage - 4, maxPage - 3, maxPage - 2, maxPage - 1, maxPage];
    } else if (currentPage < 4) {
      pagesNumber = [1, 2, 3, 4, 5];
      if (maxPage > 5) {
        pagesNumber.push(0);
      }
    } else {
      pagesNumber = [0, currentPage - 2, currentPage - 1, currentPage, currentPage + 1, currentPage + 2, 0];
    }
    let next = currentPage + 1;

    let prev = currentPage - 1;

    this.setState({ next, prev, count, currentPage, last: maxPage || 1, pagesNumber });
  }
  async handleClick(event) {
    let search = '?'
    let name = qs.parse(this.props.location.search).name;
    let where = {};
    if (name) {
      where.name = { 'contains': name }
      search += '&name=' + name
    }

    if (event) {
      search += '&page=' + event;
    }
    await this.props.history.push(search)
    this.actFetchStore(where)
  }

  onSubmit = e => {
    e.preventDefault();
    e.target.reset()
    const isValid = this.validate()
    var { account, email, password, fullName, phone } = this.state;
    if (isValid) {
      var store = {
        account: account,
        phone: phone,
        email: email,
        password: password,
        roleIds: [2],
        fullName: fullName,

      }
      api.dataService.actAddStoreRequest(store)
      this.setState({
        success: !this.state.success,
        password: '',
        account: '',
        email: '',
        phone: '',
        fullName: '',
        account: '',
      })
    }
  }
  onChange = async (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({
      [name]: value
    });
  }
  onEnterPress = (e) => {
    if (e.keyCode == 13 && e.shiftKey == false) {
      e.preventDefault();
      this.onSubmitSearch();
    }
  }
  onSubmitSearch = async e => {
    e.preventDefault();
    e.target.reset()
    var { account, storeAreaId } = this.state;
    let where = {};

    if (account) {
      where.account = { 'contains': account }
    }

    this.setState({ currentPage: 1 })
    await this.props.history.push('?page=' + 1 + '&account=' + account)
    this.actFetchStore(where)
    // this.setState({ where })

  }
  async  ChnageUserLocker(storea) {
    if (storea.locked === true) {
      var store = {
        id: storea.id,
        locked: false,
      }
      var result = await api.dataService.actUpdateStoreRequest(store);
      if (result.status === 200) {
        this.actFetchStore()
      }
      else {
        api.api.showMessage('Lỗi hệ thống ! Xin thử lại sau .')
      }
    }
    else {
      var store = {
        id: storea.id,
        locked: true,
      }
      var result = await api.dataService.actUpdateStoreRequest(store);
      if (result.status === 200) {
        this.actFetchStore()
      }
      else {
        api.api.showMessage('Lỗi hệ thống ! Xin thử lại sau .')
      }
    }

  }
  render() {
    var { account, count, password, currentPage,
      prev, next, pagesNumber, last, account, email, password, fullName, phone,
      accountError, emailError, nameError, phoneError } = this.state;
    var { listStores } = this.props;
    listStores = this.props.listStores.stores
    return (
      <Row className="animated fadeIn">
        <Col xs={12}>
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i> Bộ lọc
              </CardHeader>
            <CardBody>
              <Form onSubmit={this.onSubmitSearch}>
                <Row form>
                  <Col md={4}>
                    <FormGroup>
                      <Label for="exampleState">Tên</Label>
                      <Input type="text" id="account" name="account" value={account} onChange={this.onChange.bind(this)} />
                    </FormGroup>
                  </Col>
                  {/* <Col md={4}>
                    <FormGroup>
                      <Label for="exampleCity">Khu vực</Label>
                      <Select options={options}
                        onChange={(storeAreaId) => {
                          storeAreaId = storeAreaId || { value: '', label: '' }
                          this.setState({ storeAreaId })
                        }}
                        value={storeAreaId}
                        placeholder="Search"
                      />
                    </FormGroup>
                  </Col> */}
                </Row>
                <FormGroup className="form-actions">
                  <Button type="submit" size="sm" color="primary">Tìm kiếm</Button>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>
        <Col xs={12} >
          <Button active color="success" onClick={this.toggleSuccess} className="float-right">Thêm mới người dùng</Button>
        </Col>
        <br />
        <br />
        <br />
        <Col xs={12}>
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i> {listStores.length > 0 ? `Dữ liệu (${count} mục)` : 'Đang xử lý ...'}
            </CardHeader>
            <CardBody>
              {listStores.length > 0 ?
                <Table hover bordered responsive size="sm">
                  <thead>
                    <tr>
                      <th className="text-center">#</th>
                      <th className="text-left">Thông tin tài khoản</th>
                      <th className="text-left">Thông tin liên hệ</th>

                      <th className="text-center">Khóa tài khoản</th>
                      <th className="text-center">Quyền</th>
                    </tr>
                  </thead>
                  <tbody>
                    {listStores.map((storea, index) =>
                      <tr>
                        <th scope="row" className="text-center"><a href={`#/quanlytaikhoan/danhsach/${storea.id}`}>{storea.id}</a></th>
                        <td className="text-left">
                          <div className="small ">
                            <span className="bold">Tên tài khoản</span> |  <a href={`#/quanlytaikhoan/danhsach/${storea.id}`}>{storea.account}</a>
                          </div>

                          <div className="small ">
                            <span className="bold">Tài đầy đủ</span> | {storea.fullName}
                          </div>
                          <Button color="link" href={`#/quanlytaikhoan/danhsach/${storea.id}`}>Xem chi tiết</Button>
                        </td>
                        <td className="text-left">
                          <div className="small ">
                            <span className="bold">Email</span> | {storea.email}
                          </div>

                          <div className="small ">
                            <span className="bold">SĐT</span> | {storea.phone}
                          </div>
                        </td>
                        <td className="text-center">  <AppSwitch className={'mx-1 mt-3'} id="locked" name="locked" checked={storea.locked ? true : false} onChange={() => this.ChnageUserLocker(storea)} color={'primary'} label /></td>
                        <td className="text-center"><Alert color="info" className="mt-2" size="sm">
                          {storea.roleIds.join(',')}
                        </Alert></td>
                      </tr >
                    )}

                  </tbody>
                </Table>
                : <Alert color="warning" className="center">
                  Không có dữ liệu !
      </Alert>}
              <div style={{ display: 'table', margin: '0 auto' }}>
                {pagesNumber.length == 1 ? '' : <Pagination >
                  <PaginationItem>
                    {prev === 0 ? <PaginationLink className="hide">First</PaginationLink> :
                      <PaginationLink onClick={() => this.handleClick(1)} id="first">First</PaginationLink>
                    }
                  </PaginationItem>
                  <PaginationItem>
                    {prev < 1 ? <PaginationLink className="hide"><i className="cui-chevron-left icons "></i></PaginationLink> :
                      <PaginationLink onClick={() => this.handleClick(prev)} id="prev"><i className="cui-chevron-left icons "></i></PaginationLink>
                    }
                  </PaginationItem>
                  {!pagesNumber ? '' : pagesNumber.map((pageNumber, index) => {
                    return (<Pagination key={index}>
                      <PaginationItem active={currentPage == (pageNumber)} >
                        {pageNumber ? <PaginationLink onClick={() => { this.handleClick(pageNumber) }} key={index} >
                          {pageNumber}
                        </PaginationLink> : <PaginationLink className="disabled" onClick={() => { this.handleClick(pageNumber) }} key={index} >
                            ...
                          </PaginationLink>
                        }
                      </PaginationItem>
                    </Pagination>)

                  })
                  }

                  <PaginationItem>
                    {
                      next > last ? <PaginationLink className="hide"><i className="cui-chevron-right icons "></i></PaginationLink> :
                        <PaginationLink onClick={() => this.handleClick(next)} id="next" > <i className="cui-chevron-right icons "></i></PaginationLink>
                    }
                  </PaginationItem>

                  <PaginationItem>
                    {
                      currentPage === last ? <PaginationLink className="hide">Last</PaginationLink> :
                        <PaginationLink onClick={() => this.handleClick(last)} id="last" >Last</PaginationLink>
                    }
                  </PaginationItem>
                </Pagination>}

              </div>
            </CardBody>
          </Card>
        </Col>
        <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
          className={'modal-success ' + this.props.className}>
          <ModalHeader toggle={this.toggleSuccess}>Thêm mới người dùng</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit.bind(this)}>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '100px' }}>Tài khoản</InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" id="account" name="account" value={account} onChange={this.onChange.bind(this)} />

                </InputGroup>
                {accountError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{accountError}</i> </p> : ''}
              </FormGroup>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '100px' }}>Mật khẩu</InputGroupText>
                  </InputGroupAddon>
                  <Input type="password" id="password" name="password" value={password} onChange={this.onChange.bind(this)} />

                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '100px' }}>Email</InputGroupText>
                  </InputGroupAddon>
                  <Input type="email" id="email" name="email" value={email} onChange={this.onChange.bind(this)} />

                </InputGroup>
                {emailError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{emailError}</i> </p> : ''}
              </FormGroup>

              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '100px' }}>Số điện thoại</InputGroupText>
                  </InputGroupAddon>
                  <Input type="number" id="phone" name="phone" min='0' step='1' value={phone} onChange={this.onChange.bind(this)} />

                </InputGroup>
                {phoneError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{phoneError}</i> </p> : ''}
              </FormGroup>

              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '100px' }}>Tên đầy đủ</InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" id="fullName" name="fullName" min='0' step='1' value={fullName} onChange={this.onChange.bind(this)} />

                </InputGroup>
                {nameError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{nameError}</i> </p> : ''}
              </FormGroup>
              <FormGroup className="form-actions text-right">
                <Button type="submit" size="sm" color="primary">Lưu</Button>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </Row>
    );
  }
}
const mapStateToProps = state => {
  return {
    userReducer: state.userReducer,
    listStores: state.listStores,
  }
}
export default connect(mapStateToProps)(Accounts);


