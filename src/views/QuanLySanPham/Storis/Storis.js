import React, { Component } from 'react';
import {
  Button, Card, CardBody, CardHeader, Modal,
  ModalBody, CustomInput,
  Form,
  FormGroup,
  Row, Col,
  Input,
  InputGroup,
  InputGroupAddon, Alert,
  InputGroupText, Label,
  ModalHeader, Pagination, PaginationItem, PaginationLink, Table
} from 'reactstrap';
import axios, { post } from 'axios';
import 'react-select/dist/react-select.css'
import 'react-virtualized-select/styles.css'
import Select from "react-virtualized-select";
import { api } from '../..'
import { connect } from "react-redux";
import qs from 'query-string'
import '../../../index.css'
import ReactLoading from "react-loading";
import { Section, Title, Article, Prop, list } from "../../../controls/generic";
import ListAuthor from './listAuthor'
import FileUpload from './fileUpload'
import ListCategory from './listCategory'


class Storis extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false, isDone: false, thumbnail: '', loadding: false,
      name: '', lastChapter: 0, totalChapter: 0, tag: '', fileId: '', nameAuthor: '',
      nameError: '', authorId: '', authorName: '', isDones: false, fileName: '',
      description: '',
      descriptionError: '',
      categoryId: '',
      categoryName: '',
      categoryIdError: '',
      categories: '',
      count: 0,
      prev: 0,
      next: 2,
      last: 0,
      first: 1,
      currentPage: 1,
      pagesNumber: [], file: '',
      clearable: true, imagePreviewUrl: '',
      imageName: '', imageType: '', fileError: '', imageError: '',
      localStorageSave: JSON.parse(localStorage.getItem('categories')) || ''
    };
    this.toggleSuccess = this.toggleSuccess.bind(this);
  }

  validate = () => {
    let nameError = "";
    let descriptionError = "";
    let categoryIdError = "";
    let fileError = "";
    let imageError = "";
    let authorError = "";
    let categoryError = "";
    var { name, description, file, thumbnail, categoryId, categoryName, authorName } = this.state;
    if (!name) {
      nameError = 'Trường này không được để trống !'
    }
    if (!file) {
      fileError = 'Bạn chưa chọn File !'
    }
    if (!categoryName) {
      categoryError = 'Trường này không được để trống !'
    }
    if (!authorName) {
      authorError = 'Trường này không được để trống !'
    }
    if (!thumbnail) {
      imageError = 'Bạn chưa chọn ảnh !'
    }
    // if (!description) {
    //   descriptionError = 'Trường này không được để trống !'
    // }
    if (!categoryId) {
      categoryIdError = 'Vui lòng chọn danh mục sản phẩm !'
    }
    if (nameError || categoryIdError || fileError || imageError || authorError || categoryError) {
      this.setState({ nameError, categoryIdError, fileError, imageError, authorError, categoryError });
      return false;
    }
    return true
  }
  async handleClick(event) {
    let search = '?'
    let where = {};

    let name = qs.parse(this.props.location.search).name;
    let categoryId = qs.parse(this.props.location.search).categoryId;
    if (name) {
      where.name = { 'contains': name }
      search += '&name=' + name
    }
    console.log('kk', categoryId)
    if (categoryId) {
      // where.categoryId = categoryId
      where.categoryId = categoryId;

      search += '&categoryId=' + categoryId;
    }
    if (event) {
      search += '&page=' + event;
    }
    await this.props.history.push(search)
    //where
    this.actFetchProducts(where)
  }

  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }


  componentDidMount() {
    this.handleClick()
  }
  // handleDeleteProduct(product) {
  //   if (confirm('Bạn chắc chắn muốn xóa ?')) { //eslint-disable-line
  //     api.dataService.actDeleteProductRequest(product)
  //   }
  // }
  async actFetchProducts(where) {
    let localStorageSave = JSON.parse(localStorage.getItem('categories'))
    console.log('111', localStorageSave)
    let urlQueryObj = qs.parse(this.props.location.search);
    let currentPage = Number(urlQueryObj.page || 1);
    if (isNaN(currentPage) || currentPage < 1) {
      currentPage = 1;
    }
    let limit = urlQueryObj.limit || 10;
    let skip = (currentPage - 1) * limit;
    var result = await api.dataService.actFetchProductsRequest(skip, limit, '', where, [{ id: 'DESC' }]);
    // var result2 = await api.dataService.actFetchUsersRequest(0, 10, 'id,name,description', where, '')
    console.log('aa', result)
    // result.data = result.data.map(v => {
    //   let categoryId = v.categoryId;
    //   let nameCategory = [];
    //   categoryId.map(w => {
    //     let category = localStorageSave[w];
    //     nameCategory.push(category.name);
    //   })
    //   v.categoryName = nameCategory.join(',');
    //   return v;
    // })
    console.log({ localStorageSave });
    console.log({ resultdata: result.data })
    let count = result.count;
    let maxPage = Math.ceil(count / limit);
    let pagesNumber = [];
    if (maxPage <= 5) {
      for (let i = 0; i < maxPage; i++) {
        pagesNumber.push(i + 1);
      }
    }
    else if (maxPage > 5 && maxPage - 2 <= currentPage) {
      pagesNumber = [0, maxPage - 4, maxPage - 3, maxPage - 2, maxPage - 1, maxPage];
    } else if (currentPage < 4) {
      pagesNumber = [1, 2, 3, 4, 5];
      if (maxPage > 5) {
        pagesNumber.push(0);
      }
    } else {
      pagesNumber = [0, currentPage - 2, currentPage - 1, currentPage, currentPage + 1, currentPage + 2, 0];
    }
    let next = currentPage + 1;
    let prev = currentPage - 1;

    this.setState({ next, prev, count, currentPage, last: maxPage || 1, pagesNumber });
  }

  onChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  async onChangefile(e, id) {
    if (id) {
      await this.setState({ fileId: e.target.files[0], loadding: true, id: id })
      this.fileUpload(this.state.fileId, id).then((response) => {
        console.log(response);
        if (response.status === 200) {
          this.setState({ fileId: response.data.created[0].id, loadding: false })
          this.handleClick()
        }
      })

    } else {
      let reader = new FileReader();
      let file = e.target.files[0];
      { file ? reader.readAsDataURL(file) : api.api.showMessage('Bạn chưa chọn file', 'WARNING') }
      reader.onloadend = () => {
        this.setState({
          file: file,
          fileName: file.name,
        });
      }
      // await this.setState({ fileId: e.target.files[0], loadding: true, fileName: e.target.files[0].name, })
      // console.log('123', this.state.fileId)
      // this.fileUpload(this.state.fileId).then((response) => {
      //   console.log(response);
      //   if (response.status === 200) {
      //     this.setState({ fileId: response.data.created[0].id, loadding: false })
      //     // window.location.reload(true);
      //   }
      // })
    }
  }

  async onChangeimage(e, id) {
    if (id) {
      await this.setState({ thumbnail: e.target.files[0], loadding: true, id: id }, () => {
        console.log('aaa', this.state.thumbnail)
      })
      this.imageUpload(this.state.thumbnail, id).then((response) => {
        console.log(response);
        if (response.status === 200) {
          this.setState({ thumbnail: response.data.created[0].id, loadding: false })
          window.location.reload(true);
        }
      })
    } else {
      let reader = new FileReader();
      let thumbnail = e.target.files[0];
      { thumbnail ? reader.readAsDataURL(thumbnail) : api.api.showMessage('Bạn chưa chọn ảnh', 'WARNING') }
      reader.onloadend = () => {
        this.setState({
          thumbnail: thumbnail,
          imagePreviewUrl: reader.result,
          imageName: thumbnail.name,
          imageType: thumbnail.type,
        });
      }
    }
  }
  async handleSelectAuthor(product) {
    await this.setState({
      authorId: product.id,
      authorName: product.name,
    })
  }
  async handleSelectCategory(product) {
    await this.setState({
      categoryId: product.id,
      categoryName: product.name
    })
  }
  onSubmit = async e => {
    e.preventDefault();
    e.target.reset()
    let where = {}
    const isValid = this.validate()
    var { authorId, categoryId, name, description, lastChapter, totalChapter, isDone, fileId, thumbnail, tag } = this.state;
    if (isValid) {
      Promise.all([this.imageUpload(this.state.thumbnail), this.fileUpload(this.state.file)]).then((response) => {
        if (response[0].status === 200 && response[0].data.created.length === 1 && response[1].status === 200 && response[1].data.created.length === 1) {
          var product = {
            authorId: authorId,
            categoryId: categoryId,
            name: name,
            description: description,
            lastChapter: lastChapter,
            totalChapter: totalChapter,
            isDone: isDone,
            fileId: response[1].data.created[0].id,
            thumbnail: response[0].data.created[0].id,
            tag: tag
          }
          api.dataService.actAddProductRequest(product)
          this.setState({
            success: !this.state.success,
            authorId: '',
            categoryId: '',
            name: "",
            imagePreviewUrl: '',
            fileName: '',
            categoryName: '',
            description: "",
            authorName: '',
            lastChapter: 0,
            totalChapter: 0,
            isDone: '',
            fileId: '',
            thumbnail: '',
            file: '',
            tag: "",
            loadding: false
          })
          this.handleClick()
        }
        else {
          api.api.showMessage('Dữ liệu đang được cập nhật', 'Thông báo')
        }
      }).catch(reason => {
        console.log(reason)
      })

    }


  }
  async fileUpload(file, id) {
    let token = localStorage.getItem('RFTK-GK') ? localStorage.getItem('RFTK-GK') : api.getToken()
    let url = api.config.HOST + '/api/file-management/upload-file'
    if (id) {
      url += '?id=' + id;
    }
    const formData = new FormData();
    formData.append('files', file)
    const config = {
      headers: {
        'Authorization': token,
        'content-type': 'multipart/form-data',
        'Api-Version': 1
      }
    }
    this.setState({ loadding: true })

    return post(url, formData, config)
  }
  async imageUpload(thumbnail, id) {
    console.log(thumbnail, 'jjjj')
    let token = localStorage.getItem('RFTK-GK') ? localStorage.getItem('RFTK-GK') : api.getToken()
    let url = api.config.HOST + '/api/file-management/upload-image'
    if (id) {
      url += '?id=' + id;
    }
    const formData = new FormData();
    formData.append('images', thumbnail)
    const config = {
      headers: {
        'Authorization': token,
        'content-type': 'multipart/form-data',
        'Api-Version': 1
      }
    }
    this.setState({ loadding: true })
    return post(url, formData, config)
  }
  onEnterPress = (e) => {
    if (e.keyCode == 13 && e.shiftKey == false) {
      e.preventDefault();
      this.onSubmitSearch();
    }
  }
  onSubmitSearch = async e => {
    e.preventDefault();
    e.target.reset()
    var { name, categoryId } = this.state;
    let where = {};
    // if(name === ''){
    //   return api.api.showMessage('Chưa điền tên sản phẩm ! Vui lòng kiểm tra lại','Thông báo')
    // }

    if (name) {
      where.name = { 'contains': name }
    }
    if (categoryId) {
      console.log({ categoryId })
      // if (categoryId.length === 1) {
      //   where.or = [{ categoryId: { 'contains': categoryId[0] + ',' } }, { categoryId: { 'contains': ',' + categoryId[0] } }]
      // } else {
      //   where.and = [];
      //   categoryId.map(v => {
      //     let or = {
      //       or: [{ categoryId: { 'contains': v + ',' } }, { categoryId: { 'contains': ',' + v } }]
      //     }
      //     where.and.push(or)
      //   })
      // }
      where.categoryId = categoryId;
    }

    this.setState({ currentPage: 1 })
    await this.props.history.push('?page=' + 1 + '&name=' + name + '&categoryId=' + categoryId)
    this.actFetchProducts(where)

  }
  render() {
    var { listProducts } = this.props;
    listProducts = this.props.listProducts.products
    var { listAuthor } = this.props;
    listAuthor = this.props.listAuthor.users
    var { listCategories } = this.props;
    listCategories = this.props.listCategories.categories
    var { name, imageName, imageType, count, authorId, authorName, fileName, fileError, imageError,
      lastChapter, totalChapter, description, isDone, tag, categoryId, categoryName, localStorageSave, categoryError, authorError,
      currentPage, prev, next, pagesNumber, last, nameError } = this.state;
    localStorageSave = Object.entries(localStorageSave).map((value) => (value));
    // const option1 = localStorageSave.length > 0 ? localStorageSave.map((a) => {
    //   return (
    //     { label: a[1].name, value: a[1].id, key: a[1].id }

    //   )
    // }) : ''
    console.log('111', fileName)
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Bộ lọc
              </CardHeader>
              <CardBody>
                <Form onSubmit={this.onSubmitSearch}>
                  <Row form>
                    <Col md={4}>
                      <FormGroup>
                        <Label for="exampleState">Tên truyện</Label>
                        <Input type="text" id="name" name="name" value={name} onChange={this.onChange.bind(this)} />
                      </FormGroup>
                    </Col>
                    <Col md={4}>
                      <Label for="exampleCity" style={{ opacity: 0 }}>Danh mục truyện</Label>
                      <ListCategory handleSelectCategory={this.handleSelectCategory.bind(this)} listCategories={listCategories} location={this.props.location} history={this.props.history} categoryName={categoryName} categoryId={categoryId} />
                    </Col>
                  </Row>
                  <FormGroup className="form-actions">
                    <Button type="submit" size="sm" color="primary">Tìm kiếm</Button>
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>
          <br />
          <br />
          <br />

          <Col xs={12} >
            <Button active color="success" onClick={this.toggleSuccess} className="float-right">Thêm truyện tranh</Button>
          </Col>
          <br />
          <br />
          <br />
          <Col xs={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> {listProducts.length > 0 ? `Danh sách sản phẩm (${count} mục)` : 'Đang xử lý ...'}
              </CardHeader>
              <CardBody>
                {listProducts.length > 0 ?
                  <Table responsive bordered >
                    <thead>
                      <tr>
                        <th className="text-center">#</th>
                        <th>Thông tin chung</th>
                        <th>Thông tin tác giả</th>
                        <th className="w-10">Thông tin File</th>
                        <th className="text-center">Hình ảnh</th>
                      </tr>
                    </thead>
                    <tbody>
                      {listProducts.map((product, index) =>
                        <tr key={index}>
                          <th scope="row" className="text-center"><a href={`#/quanlysanpham/danhsach/${product.id}`}>{product.id}</a></th>
                          <td className="relative">
                            <div className="small ">
                              <span className="bold">Tên</span> | {product.name}
                            </div>
                            <div className="small ">
                              <span className="bold">Mô tả</span> | {(product.description || "").toString().substr(0, 100) + ((product.description || "").toString().length < 100 ? '' : ' ...')}
                            </div>
                            <div className="small ">
                              <span className="bold">Danh mục</span> | {product.categoryId.name}
                            </div>
                            <div className="small ">
                              <span className="bold">Chương</span> | <span>{product.lastChapter || '?'}/{product.totalChapter || '?'}</span>
                            </div>
                            <div className="bottom-table">
                              <Button color="link" outline className="mt-2" href={`#/quanlysanpham/danhsach/${product.id}`} >Xem chi tiết</Button>
                              {/* <Button color="link" size="sm" className="mt-2" color="danger" onClick={() => { this.handleDeleteProduct(product.id) }}  >Xóa</Button> */}
                            </div>

                          </td>
                          <td className="relative">
                            <div className="small ">
                              <span className="bold">Tên</span> | {product.authorId.name}
                            </div>
                            <div className="small ">
                              <span className="bold">Mô tả</span> | {(product.authorId.description || "").toString().substr(0, 100) + ((product.authorId.description || "").toString().length < 100 ? '' : ' ...')}
                            </div>
                            <Button color="link" href={`#/users/${product.authorId.id}`} outline className=" mt-2 bottom-table" size="sm">Xem chi tiết</Button></td>
                          <td className="relative">
                            {this.state.loadding && this.state.id == product.fileId.id ?
                              <Article style={{ display: 'table', margin: '0 auto' }}>
                                <ReactLoading type="spinningBubbles" color="#4acc7096" />
                              </Article> :
                              <React.Fragment>
                                <div className="small ">
                                  <span className="bold">Original Filename</span> | {product.fileId.fileName}
                                </div>
                                <div className="small ">
                                  <span className="bold">Kiểu File</span> | {product.fileId.fileType}
                                </div>
                                <div className="small ">
                                  <span className="bold">Server FileName</span> | {product.fileId.serverFileName}
                                </div>
                              </React.Fragment>}

                            <div className="bottom-table text-center mt-2 mb-2 mr-2 ">
                              <Label >Cập nhật File</Label>
                              {/* <Button color="link" outline outline className=" mt-2" size="sm" onClick={() => { imageUpload(product.thumbnail.id) }}>Cập nhật ảnh</Button> */}
                              <CustomInput type="file" accept=" .epub" id="fileId" name="fileId" size="sm" onChange={(data) => this.onChangefile(data, product.fileId.id)} />
                            </div>
                          </td>
                          <td className="text-center " style={{ width: '250px' }}>
                            {this.state.loadding && this.state.id == product.thumbnail.id ?
                              <Article style={{ display: 'table', margin: '0 auto' }}>
                                <ReactLoading type="spin" color="#4acc7096" />
                              </Article> : <img src={product.thumbnail.url} className="img-avatar-user" alt={product.thumbnail.fileName} style={{ width: '150px', height: '150px' }} />}

                            <Label className="mt-2">Cập nhật ảnh</Label>
                            {/* <Button color="link" outline outline className=" mt-2" size="sm" onClick={() => { imageUpload(product.thumbnail.id) }}>Cập nhật ảnh</Button> */}
                            <CustomInput type="file" accept=".jpg, .jpeg" id="thumbnails" name="thumbnail" size="sm" onChange={(data) => this.onChangeimage(data, product.thumbnail.id)} />
                          </td>
                        </tr >

                      )}
                    </tbody>
                  </Table> : <Alert color="warning" className="center">
                    Không có dữ liệu !
      </Alert>}
                <div style={{ display: 'table', margin: '0 auto' }}>
                  {pagesNumber.length == 1 ? '' : <Pagination >
                    <PaginationItem>
                      {prev === 0 ? <PaginationLink className="hide">First</PaginationLink> :
                        <PaginationLink onClick={() => this.handleClick(1)} id="first">First</PaginationLink>
                      }
                    </PaginationItem>
                    <PaginationItem>
                      {prev < 1 ? <PaginationLink className="hide"><i className="cui-chevron-left icons "></i></PaginationLink> :
                        <PaginationLink onClick={() => this.handleClick(prev)} id="prev"><i className="cui-chevron-left icons "></i></PaginationLink>
                      }
                    </PaginationItem>
                    {!pagesNumber ? '' : pagesNumber.map((pageNumber, index) => {
                      return (<Pagination key={index}>
                        <PaginationItem active={currentPage == (pageNumber)} >
                          {pageNumber ? <PaginationLink onClick={() => { this.handleClick(pageNumber) }} key={index} >
                            {pageNumber}
                          </PaginationLink> : <PaginationLink className="disabled" onClick={() => { this.handleClick(pageNumber) }} key={index} >
                              ...
                          </PaginationLink>
                          }
                        </PaginationItem>
                      </Pagination>)

                    })
                    }

                    <PaginationItem>
                      {
                        next > last ? <PaginationLink className="hide"><i className="cui-chevron-right icons "></i></PaginationLink> :
                          <PaginationLink onClick={() => this.handleClick(next)} id="next" > <i className="cui-chevron-right icons "></i></PaginationLink>
                      }
                    </PaginationItem>

                    <PaginationItem>
                      {
                        currentPage === last ? <PaginationLink className="hide">Last</PaginationLink> :
                          <PaginationLink onClick={() => this.handleClick(last)} id="last" >Last</PaginationLink>
                      }
                    </PaginationItem>
                  </Pagination>}

                </div>
              </CardBody>
            </Card>
          </Col>
          <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
            className={'modal-success ' + this.props.className}>
            <ModalHeader toggle={this.toggleSuccess}>Thêm truyện tranh</ModalHeader>
            <ModalBody>
              <Form onSubmit={this.onSubmit}>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '100px' }}>Tên truyện</InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" id="name" name="name" value={name} onChange={this.onChange.bind(this)} />

                  </InputGroup>
                  {nameError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{nameError}</i> </p> : ''}
                </FormGroup>
                <ListCategory categoryError={categoryError} handleSelectCategory={this.handleSelectCategory.bind(this)} listCategories={listCategories} location={this.props.location} history={this.props.history} categoryName={categoryName} categoryId={categoryId} />

                <ListAuthor authorError={authorError} handleSelectAuthor={this.handleSelectAuthor.bind(this)} authorName={authorName} authorId={authorId} location={this.props.location} history={this.props.history} listAuthor={listAuthor} />
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '100px' }}>Mô tả</InputGroupText>
                    </InputGroupAddon>
                    <Input type="textarea" rows={4} id="description" name="description" value={description} onChange={this.onChange.bind(this)} />

                  </InputGroup>
                  {/* {nameError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{nameError}</i> </p> : ''} */}
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '100px' }}>Chương </InputGroupText>
                    </InputGroupAddon>
                    <Input type="number" min="0" id="lastChapter" name="lastChapter" value={lastChapter} onChange={this.onChange.bind(this)} />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '100px' }}>Tổng chương</InputGroupText>
                    </InputGroupAddon>
                    <Input type="number" min="0" id="totalChapter" name="totalChapter" value={totalChapter} onChange={this.onChange.bind(this)} />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '100px' }}>Tag</InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" id="tag" name="tag" value={tag} onChange={this.onChange.bind(this)} />

                  </InputGroup>
                </FormGroup>
                {/* <FileUpload id={'upload'} fileError={fileError} onChangefiles={() => { this.onChangefiles.bind(this)}} /> */}
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '100px' }}>File epub</InputGroupText>
                    </InputGroupAddon>
                    <CustomInput type="file" id="fileIds" accept=".epub" name="fileId" onChange={this.onChangefile.bind(this)} />

                  </InputGroup>
                  {fileError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{fileError}</i> </p> : ''}
                </FormGroup>
                {this.state.fileName ? <div style={{ width: '100%', height: 'auto', marginBottom: '13px', textAlign: 'center' }} >
                  <div className="small ">
                    <span className="bold">Name</span> | <span>{fileName}</span>
                  </div>


                </div> : ''}
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '100px' }}>Ảnh bìa</InputGroupText>
                    </InputGroupAddon>
                    <CustomInput type="file" id="thumbnail" accept=".png,.jpg, .jpeg" name="thumbnail" onChange={this.onChangeimage.bind(this)} />

                  </InputGroup>
                  {imageError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{imageError}</i> </p> : ''}
                </FormGroup>
                <FormGroup row>
                  <Col md="8">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '100px' }}>Trạng thái</InputGroupText>
                    </InputGroupAddon>
                  </Col>
                  <Col md="4">
                    <CustomInput className="mt-2" type="checkbox" id="isDone" name="isDone" label="Đã hoàn thành" checked={isDone} onChange={this.onChange.bind(this)} inline />
                  </Col>
                </FormGroup>
                {this.state.imagePreviewUrl ? <div className="gallery-append" style={{ width: '100%' }} >
                  <img src={this.state.imagePreviewUrl} style={{ width: '100px', height: '100px' }} className="float-left" />
                  <div className="float-right"  >
                    <div className="small ">
                      <span className="bold">Name</span> | <span>{imageName}</span>
                    </div>
                    <div className="small ">
                      <span className="bold">Type</span> |<span>{imageType}</span>
                    </div>
                  </div>

                </div> : ''}
                <FormGroup className="form-actions float-right mt-2">
                  <Button type="submit" size="sm" color="primary" >{this.state.loadding ? <Article style={{ display: 'table', margin: '0 auto', width: '10px', height: '10px' }}>
                    <ReactLoading type="spin" color="#4acc7096" />
                  </Article> : 'Thêm'}</Button>
                </FormGroup>
              </Form>
            </ModalBody>
          </Modal>
        </Row>
      </div >
    );
  }
}

const mapStateToProps = state => {
  return {
    userReducer: state.userReducer,
    listProducts: state.listProducts,
    listAuthor: state.listAuthor,
    listCategories: state.listCategories,
  }
}
export default connect(mapStateToProps)(Storis);