import React, { Component } from 'react';
import {
    Button, Card, CardBody, CardHeader, Modal,
    ModalBody,
    Form,
    FormGroup,
    Row, Col,
    Input,
    Alert,
    Label,
    ModalHeader, Pagination, PaginationItem, PaginationLink, Table
} from 'reactstrap';
import 'react-select/dist/react-select.css'
import 'react-virtualized-select/styles.css'
import { api } from '../../../views'
import qs from 'query-string'
import '../../../index.css'
class ListAuthor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            large: false,
            nameAuthor: '',
            name: '',
            authorError: '',
            description: '',
            descriptionError: '',
            count: 0,
            prev: 0,
            next: 2,
            last: 0,
            first: 1,
            currentPage: 1,
            pagesNumber: [],
            isSelect: false
        };
        this.toggleLarge = this.toggleLarge.bind(this);
    }

    async handleClickAuthor(event) {
        let search = '?'
        let where = {};

        let name = qs.parse(this.props.location.search).nameAuthor;
        if (name) {
            where.name = { 'contains': name }
            search += '&name=' + name
        }
        if (event) {
            search += '&page=' + event;
        }
        await this.props.history.push(search)
        this.actFetchAuthors(where)
    }

    toggleLarge() {
        this.setState({
            large: !this.state.large,
        });
        this.handleClickAuthor()
    }

    async actFetchAuthors(where) {
        let urlQueryObj = qs.parse(this.props.location.search);
        let currentPage = Number(urlQueryObj.page || 1);
        if (isNaN(currentPage) || currentPage < 1) {
            currentPage = 1;
        }
        let limit = urlQueryObj.limit || 10;
        let skip = (currentPage - 1) * limit;
        var result = await api.dataService.actFetchUsersRequest(skip, limit, 'id,name,description', where, [{id:'DESC'}])
        let count = result.count;
        let maxPage = Math.ceil(count / limit);
        let pagesNumber = [];
        if (maxPage <= 5) {
            for (let i = 0; i < maxPage; i++) {
                pagesNumber.push(i + 1);
            }
        }
        else if (maxPage > 5 && maxPage - 2 <= currentPage) {
            pagesNumber = [0, maxPage - 4, maxPage - 3, maxPage - 2, maxPage - 1, maxPage];
        } else if (currentPage < 4) {
            pagesNumber = [1, 2, 3, 4, 5];
            if (maxPage > 5) {
                pagesNumber.push(0);
            }
        } else {
            pagesNumber = [0, currentPage - 2, currentPage - 1, currentPage, currentPage + 1, currentPage + 2, 0];
        }
        let next = currentPage + 1;
        let prev = currentPage - 1;

        this.setState({ next, prev, count, currentPage, last: maxPage || 1, pagesNumber });
    }

    onChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }



    onEnterPress = (e) => {
        if (e.keyCode == 13 && e.shiftKey == false) {
            e.preventDefault();
            this.onSubmitSearchAuthor();
        }
    }

    onSubmitSearchAuthor = async e => {
        e.preventDefault();
        e.target.reset()
        var { nameAuthor } = this.state;
        let where = {};

        if (nameAuthor) {
            where.name = { 'contains': nameAuthor }
        }
        this.setState({ currentPage: 1 })
        await this.props.history.push('?page=' + 1 + '&nameAuthor=' + nameAuthor)
        this.actFetchAuthors(where)
    }

    render() {
        var { listAuthor, authorId, authorName, authorError } = this.props;
        var { currentPage, prev, next, pagesNumber, last, count, nameAuthor } = this.state;
        console.log('yyy', authorName)
        return (
            <React.Fragment>
                <Row form>
                    <Col md={8}>
                        <FormGroup>
                            <Input type="text" name="authorId" id="authorId" disabled value={authorName} placeholder="Chọn tác giả" />
                            {authorError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{authorError}</i> </p> : ''}
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <Button onClick={this.toggleLarge} className="mr-1 mb-3" color="primary">Chọn tác giả</Button>
                        </FormGroup>
                    </Col>
                </Row>
                <Modal isOpen={this.state.large} toggle={this.toggleLarge}
                    className={'modal-lg ' + this.props.className}>
                    <ModalHeader toggle={this.toggleLarge}>Chọn tác giả</ModalHeader>
                    <ModalBody>
                        <Col xs={12}>
                            <Card>
                                <CardHeader>
                                    <i className="fa fa-align-justify"></i> Bộ lọc
              </CardHeader>
                                <CardBody>
                                    <Form onSubmit={this.onSubmitSearchAuthor}>
                                        <Row form>
                                            <Col md={4}>
                                                <FormGroup>
                                                    <Label for="exampleState">Tên tác giả</Label>
                                                    <Input type="text" id="nameAuthor" name="nameAuthor" value={nameAuthor} onChange={this.onChange.bind(this)} />
                                                </FormGroup>
                                            </Col>


                                        </Row>
                                        <FormGroup className="form-actions text-right">
                                            <Button type="submit" size="sm" color="primary">Tìm kiếm</Button>
                                        </FormGroup>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs={12}>
                            <Card>
                                <CardHeader>
                                    <i className="fa fa-align-justify"></i> {listAuthor.length > 0 ? `Dữ liệu (${count} mục)` : 'Đang xử lý ...'}
                                </CardHeader>
                                <CardBody>
                                    {listAuthor.length > 0 ?
                                        <Table responsive bordered >
                                            <thead>
                                                <tr>
                                                    <th className="text-center">ID</th>
                                                    <th>Tên tác giả</th>
                                                    <th className="text-center">Hành động</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                {listAuthor.map((product, index) =>
                                                    <tr key={index}>
                                                        <th scope="row" className="text-center"><a href={`#/quanlysanpham/danhsach/${product.id}`}>{product.id}</a></th>
                                                        <td className="relative">
                                                            <div className="small ">
                                                                <span className="bold">Tên</span> | {product.name}
                                                            </div>
                                                            <div className="small ">
                                                                <span className="bold">Mô tả</span> | {(product.description ||"").toString().substr(0,100)+((product.description ||"").toString().length<100?'':' ...')}
                                                            </div>
                                                        </td>
                                                        <td className="relative " >
                                                            <Button color="danger" className="w-100"
                                                                onClick={() => this.props.handleSelectAuthor(product)}>
                                                                {!(authorId === product.id) ? 'Chọn' : 'Bỏ chọn'}</Button>{' '}
                                                        </td>
                                                    </tr >

                                                )}
                                            </tbody>
                                        </Table> : <Alert color="warning" className="center">
                                            Không có dữ liệu !
      </Alert>}
                                    <div style={{ display: 'table', margin: '0 auto' }}>
                                        {pagesNumber.length == 1 ? '' : <Pagination >
                                            <PaginationItem>
                                                {prev === 0 ? <PaginationLink className="hide">First</PaginationLink> :
                                                    <PaginationLink onClick={() => this.handleClickAuthor(1)} id="first">First</PaginationLink>
                                                }
                                            </PaginationItem>
                                            <PaginationItem>
                                                {prev < 1 ? <PaginationLink className="hide"><i className="cui-chevron-left icons "></i></PaginationLink> :
                                                    <PaginationLink onClick={() => this.handleClickAuthor(prev)} id="prev"><i className="cui-chevron-left icons "></i></PaginationLink>
                                                }
                                            </PaginationItem>
                                            {!pagesNumber ? '' : pagesNumber.map((pageNumber, index) => {
                                                return (<Pagination key={index}>
                                                    <PaginationItem active={currentPage == (pageNumber)} >
                                                        {pageNumber ? <PaginationLink onClick={() => { this.handleClickAuthor(pageNumber) }} key={index} >
                                                            {pageNumber}
                                                        </PaginationLink> : <PaginationLink className="disabled" onClick={() => { this.handleClickAuthor(pageNumber) }} key={index} >
                                                                ...
                          </PaginationLink>
                                                        }
                                                    </PaginationItem>
                                                </Pagination>)

                                            })
                                            }

                                            <PaginationItem>
                                                {
                                                    next > last ? <PaginationLink className="hide"><i className="cui-chevron-right icons "></i></PaginationLink> :
                                                        <PaginationLink onClick={() => this.handleClickAuthor(next)} id="next" > <i className="cui-chevron-right icons "></i></PaginationLink>
                                                }
                                            </PaginationItem>

                                            <PaginationItem>
                                                {
                                                    currentPage === last ? <PaginationLink className="hide">Last</PaginationLink> :
                                                        <PaginationLink onClick={() => this.handleClickAuthor(last)} id="last" >Last</PaginationLink>
                                                }
                                            </PaginationItem>
                                        </Pagination>}

                                    </div>
                                </CardBody>
                            </Card>
                            <Button color="secondary" className="float-right" color="primary" onClick={this.toggleLarge}>Đóng</Button>
                        </Col>
                    </ModalBody>
                </Modal>
            </React.Fragment>
        );
    }
}


export default ListAuthor;