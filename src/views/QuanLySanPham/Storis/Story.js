import React, { Component } from 'react';
import {
  Container, Card, CardBody, Alert, Col, Row, Button,
  Form, CustomInput, ModalBody, Label, Table,
  FormGroup, Modal, ModalHeader, CardHeader,
  Input, Pagination, PaginationItem, PaginationLink,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from 'reactstrap';
import qs from 'query-string'
import { api } from '../../../views';
import { connect } from "react-redux";
import Select from "react-virtualized-select";
import ListCategory from './listCategory'


class Story extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '', clearable: true, large: false, categoryName: '',
      description: '', listAuthor: '', totalChapter: 0,
      nameError: '', isSelect: false, authorId: '',
      descriptionError: '',
      categoryId: '',
      categories: '',
      authorId: '', isDone: '', tag: '', nameAuthor: '',authorName:'',
      localStorageSave: JSON.parse(localStorage.getItem('categories')) || '',
      count2: 0,
      prev2: 0,
      next2: 2,
      last2: 0,
      first2: 1,
      currentPage2: 1,
      pagesNumber2: [],
    }
    this.toggleLarge = this.toggleLarge.bind(this);
  }

  toggleLarge() {
    this.setState({
      large: !this.state.large,
    });
  }
  validate = () => {
    let nameError = "";
    let authorError = "";
    let categoryError = "";
    let descriptionError = "";
    var { name, description, categoryName, authorName } = this.state;
    if (!name) {
      nameError = 'Trường này không được để trống !'
    }
    if (!categoryName) {
      categoryError = 'Trường này không được để trống !'
    }
    if (!authorName) {
      authorError = 'Trường này không được để trống !'
    }
    if (!description) {
      descriptionError = 'Trường này không được để trống !'
    }
    if (nameError || descriptionError || authorError || categoryError) {
      this.setState({ nameError, descriptionError, categoryError, authorError });
      return false;
    }
    return true
  }


  onChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }
  async handleSelectCategory(product) {
    await this.setState({
      categoryId: product.id,
      categoryName: product.name
    })
  }
  componentDidMount() {
    this.handleClickAuthor()
  }
  async handleClickAuthor(event) {
    let search = '?'
    let where = {};

    let name = qs.parse(this.props.location.search).name;
    if (name) {
      where.name = { 'contains': name }
      search += '&name=' + name
    }
    if (event) {
      search += '&page=' + event;
    }
    await this.props.history.push(search)
    this.actGetProduct(where)
  }

  async actGetProduct(where) {
    var id = this.props.match.params.id
    let urlQueryObj = qs.parse(this.props.location.search);
    let currentPage2 = Number(urlQueryObj.page || 1);
    if (isNaN(currentPage2) || currentPage2 < 1) {
      currentPage2 = 1;
    }
    let limit = urlQueryObj.limit || 10;
    let skip = (currentPage2 - 1) * limit;
    var result = await api.dataService.actFetchProductsRequest('', '', '', { id: id }, [{id:'DESC'}])
    console.log('aaa', result)
    var result3 = await api.dataService.actFetchUsersRequest(skip, limit, 'name,id', where, '')


    let count2 = result3.count;
    let maxPage = Math.ceil(count2 / limit);
    let pagesNumber2 = [];
    if (maxPage <= 5) {
      for (let i = 0; i < maxPage; i++) {
        pagesNumber2.push(i + 1);
      }
    }
    else if (maxPage > 5 && maxPage - 2 <= currentPage2) {
      pagesNumber2 = [0, maxPage - 4, maxPage - 3, maxPage - 2, maxPage - 1, maxPage];
    } else if (currentPage2 < 4) {
      pagesNumber2 = [1, 2, 3, 4, 5];
      if (maxPage > 5) {
        pagesNumber2.push(0);
      }
    } else {
      pagesNumber2 = [0, currentPage2 - 2, currentPage2 - 1, currentPage2, currentPage2 + 1, currentPage2 + 2, 0];
    }
    let next2 = currentPage2 + 1;
    let prev2 = currentPage2 - 1;
    this.setState({ listAuthor: result3.data, next2, prev2, count2, currentPage2, last2: maxPage || 1, pagesNumber2 })
    if (result.data.length === 1)
      this.setState({
        name: result.data[0].name,
        authorId: result.data[0].authorId.id,
        authorName: result.data[0].authorId.name,

        categoryId: result.data[0].categoryId.id,
        categoryName: result.data[0].categoryId.name,

        description: result.data[0].description,
        isDone: result.data[0].isDone,
        tag: result.data[0].tag,
        lastChapter: result.data[0].lastChapter,
        totalChapter: result.data[0].totalChapter
      })
  }
  handleSelectAuthor(product) {
    console.log('jjj', product)
    this.setState({
      isSelect: !this.state.isSelect,
      authorId: product.id,
      authorName: product.name
    })
  }
  onSubmit = async e => {
    e.preventDefault();
    e.target.reset()
    const isValid = this.validate()
    var id = this.props.match.params.id
    var { name, authorId, categoryId, totalChapter, description, isDone, lastChapter, tag } = this.state;
    if (isValid) {
      var product = {
        id: id,
        name: name,
        authorId: authorId,
        categoryId: categoryId,
        description: description,
        isDone: isDone,
        tag: tag,
        lastChapter: lastChapter,
        totalChapter: totalChapter
      }
      await api.dataService.actUpdateProductRequest(product);
      this.props.history.goBack(-1)
    }

  }
  onSubmitSearchAuthor = async e => {
    e.preventDefault();
    e.target.reset()
    var { nameAuthor } = this.state;
    let where = {};

    if (nameAuthor) {
      where.name = { 'contains': nameAuthor }
    }

    this.setState({ currentPage: 1 })
    await this.props.history.push('?page=' + 1 + '&name=' + nameAuthor)
    this.actGetProduct(where)

  }
  render() {
    var { description, name, categoryName, lastChapter, totalChapter, categoryError, authorError,authorName,
      localStorageSave, categoryId, nameAuthor, nameError, descriptionError, tag, currentPage2, prev2, next2, pagesNumber2, last2, count2, listAuthor, data, fileId, isDone, thumbnail } = this.state;
    var { listStores } = this.props;
    listStores = this.props.listStores.stores
    var { listCategories } = this.props;
    listCategories = this.props.listCategories.categories
    localStorageSave = Object.entries(localStorageSave).map((value) => (value));
   
    return (
      <Container>
        <Row className="justify-content-center animated fadeIn">
          <Col md="8">
            <Card className="mx-4">
              <CardBody className="p-4">
                <Alert color="warning" className="text-center">
                  Cập nhật
      </Alert>
                <Form onSubmit={this.onSubmit}>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '100px' }}>Tên truyện</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="name" name="name" value={name} onChange={this.onChange.bind(this)} />

                    </InputGroup>
                    {nameError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{nameError}</i> </p> : ''}
                  </FormGroup>
                  <ListCategory categoryError={categoryError} handleSelectCategory={this.handleSelectCategory.bind(this)} listCategories={listCategories} location={this.props.location} history={this.props.history} categoryName={categoryName} categoryId={categoryId} />

                  <Row form>
                    <Col md={8}>
                      <FormGroup>

                        <Input type="text" name="authorId" id="authorId" disabled value={this.state.authorName} placeholder="Chọn tác giả" />
                      </FormGroup>
                    </Col>
                    <Col md={3}>
                      <FormGroup>
                        <Button onClick={this.toggleLarge} className="mr-1 mb-3" color="primary">Chọn tác giả</Button>
                      </FormGroup>
                    </Col>
                    {authorError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{authorError}</i> </p> : ''}
                  </Row>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '100px' }}>Mô tả</InputGroupText>
                      </InputGroupAddon>
                      <Input type="textarea" rows={4} id="description" name="description" value={description} onChange={this.onChange.bind(this)} />

                    </InputGroup>
                    {nameError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{nameError}</i> </p> : ''}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '100px' }}>Chương</InputGroupText>
                      </InputGroupAddon>
                      <Input type="number" min="0" id="lastChapter" name="lastChapter" value={lastChapter} onChange={this.onChange.bind(this)} />

                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '100px' }}>Tổng chương</InputGroupText>
                      </InputGroupAddon>
                      <Input type="number" min="0" id="totalChapter" name="totalChapter" value={totalChapter} onChange={this.onChange.bind(this)} />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '100px' }}>Tag</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="tag" name="tag" value={tag} onChange={this.onChange.bind(this)} />

                    </InputGroup>
                    {descriptionError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{descriptionError}</i> </p> : ''}
                  </FormGroup>
                  <FormGroup row>
                    <Col md="8">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '100px' }}>Trạng thái</InputGroupText>
                      </InputGroupAddon>
                    </Col>
                    <Col md="4">
                      <CustomInput className="mt-2" type="checkbox" id="isDone" name="isDone" label="Đã hoàn thành" checked={isDone} onChange={this.onChange.bind(this)} inline />
                    </Col>
                  </FormGroup>
                  <FormGroup className="form-actions float-right">
                    <Button type="submit" size="sm" color="primary">Lưu</Button>
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Modal isOpen={this.state.large} toggle={this.toggleLarge}
          className={'modal-lg ' + this.props.className}>
          <ModalHeader toggle={this.toggleLarge}>Chọn tác giả</ModalHeader>
          <ModalBody>
            <Col xs={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Bộ lọc
              </CardHeader>
                <CardBody>
                  <Form onSubmit={this.onSubmitSearchAuthor}>
                    <Row form>
                      {/* <Col md={4}>
                      <FormGroup>
                        <Label for="exampleCity">Mã</Label>
                        <Input type="text" id="id" name="id" value={id} onChange={this.onChange.bind(this)} />
                      </FormGroup>
                    </Col> */}
                      <Col md={4}>
                        <FormGroup>
                          <Label for="exampleState">Tên tác giả</Label>
                          <Input type="text" id="nameAuthor" name="nameAuthor" value={nameAuthor} onChange={this.onChange.bind(this)} />
                        </FormGroup>
                      </Col>


                    </Row>
                    <FormGroup className="form-actions text-right">
                      <Button type="submit" size="sm" color="primary">Tìm kiếm</Button>
                    </FormGroup>
                  </Form>
                </CardBody>
              </Card>
            </Col>
            <Col xs={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> {listAuthor.length > 0 ? `Dữ liệu (${count2} mục)` : 'Đang xử lý ...'}
                </CardHeader>
                <CardBody>
                  {listAuthor.length > 0 ?
                    <Table responsive bordered >
                      <thead>
                        <tr>
                          <th className="text-center">ID</th>
                          <th>Tên danh mục</th>
                          <th className="text-center">Hành động</th>

                        </tr>
                      </thead>
                      <tbody>
                        {listAuthor.map((product, index) =>
                          <tr >
                            <th scope="row" className="text-center"><a href={`#/quanlysanpham/danhsach/${product.id}`}>{product.id}</a></th>
                            <td className="relative">
                              <div className="small ">
                                <span className="bold">Tên</span> | {product.name}
                              </div>
                              <div className="small ">
                                <span className="bold">Mô tả</span> | {(product.description ||"").toString().substr(0,100)+((product.description ||"").toString().length<100?'':' ...')}
                              </div>
                            </td>
                            <td className="relative " >
                              <Button color="danger" className="w-100"
                                onClick={() => this.handleSelectAuthor(product)}>
                                {!(this.state.authorId === product.id) ? 'Chọn' : 'Bỏ chọn'}</Button>{' '}
                            </td>
                          </tr >

                        )}
                      </tbody>
                    </Table> : <Alert color="warning" className="center">
                      Không có dữ liệu !
      </Alert>}
                  <div style={{ display: 'table', margin: '0 auto' }}>
                    {pagesNumber2.length == 1 ? '' : <Pagination >
                      <PaginationItem>
                        {prev2 === 0 ? <PaginationLink className="hide">First</PaginationLink> :
                          <PaginationLink onClick={() => this.handleClickAuthor(1)} id="first">First</PaginationLink>
                        }
                      </PaginationItem>
                      <PaginationItem>
                        {prev2 < 1 ? <PaginationLink className="hide"><i className="cui-chevron-left icons "></i></PaginationLink> :
                          <PaginationLink onClick={() => this.handleClickAuthor(prev2)} id="prev"><i className="cui-chevron-left icons "></i></PaginationLink>
                        }
                      </PaginationItem>
                      {!pagesNumber2 ? '' : pagesNumber2.map((pageNumber, index) => {
                        return (<Pagination key={index}>
                          <PaginationItem active={currentPage2 == (pageNumber)} >
                            {pageNumber ? <PaginationLink onClick={() => { this.handleClickAuthor(pageNumber) }} key={index} >
                              {pageNumber}
                            </PaginationLink> : <PaginationLink className="disabled" onClick={() => { this.handleClickAuthor(pageNumber) }} key={index} >
                                ...
                          </PaginationLink>
                            }
                          </PaginationItem>
                        </Pagination>)

                      })
                      }

                      <PaginationItem>
                        {
                          next2 > last2 ? <PaginationLink className="hide"><i className="cui-chevron-right icons "></i></PaginationLink> :
                            <PaginationLink onClick={() => this.handleClickAuthor(next2)} id="next" > <i className="cui-chevron-right icons "></i></PaginationLink>
                        }
                      </PaginationItem>

                      <PaginationItem>
                        {
                          currentPage2 === last2 ? <PaginationLink className="hide">Last</PaginationLink> :
                            <PaginationLink onClick={() => this.handleClickAuthor(last2)} id="last" >Last</PaginationLink>
                        }
                      </PaginationItem>
                    </Pagination>}

                  </div>
                </CardBody>
              </Card>
              <Button color="secondary" className="float-right" color="primary" onClick={this.toggleLarge}>Đóng</Button>
            </Col>
          </ModalBody>

        </Modal>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    userReducer: state.userReducer,
    listStores: state.listStores,
    listCategories: state.listCategories,

  }
}
export default connect(mapStateToProps)(Story);

