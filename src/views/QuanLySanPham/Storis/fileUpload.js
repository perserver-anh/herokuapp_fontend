import React, { Component } from 'react';
import {
  Button, Card, CardBody, CardHeader, Modal,
  ModalBody, CustomInput,
  Form,
  FormGroup,
  Row, Col,
  Input,
  InputGroup,
  InputGroupAddon, Alert,
  InputGroupText, Label,
  ModalHeader, Pagination, PaginationItem, PaginationLink, Table
} from 'reactstrap';
import axios, { post } from 'axios';
import 'react-select/dist/react-select.css'
import 'react-virtualized-select/styles.css'
import Select from "react-virtualized-select";
import { api } from '../..'
import { connect } from "react-redux";
import qs from 'query-string'
import '../../../index.css'
import ReactLoading from "react-loading";
import { Section, Title, Article, Prop, list } from "../../../controls/generic";
import ListAuthor from './listAuthor'
import ListCategory from './listCategory'


class FileUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }


 

  render() {
    var { fileError } = this.props;
    return (
      <React.Fragment>
      <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '100px' }}>File epub</InputGroupText>
                    </InputGroupAddon>
                    <CustomInput type="file" id={this.props.id} accept=".epub" style={{ visibility: 'hidden' }} name="fileId" onChange={this.props.onChangefiles} />

                  </InputGroup>
                  {fileError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{fileError}</i> </p> : ''}
                </FormGroup>
      </React.Fragment>

    );
  }
}

export default FileUpload;