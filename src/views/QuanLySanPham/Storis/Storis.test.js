import React from 'react';
import ReactDOM from 'react-dom';
import Storis from './Storis';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Storis />, div);
  ReactDOM.unmountComponentAtNode(div);
});
