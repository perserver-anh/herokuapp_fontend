import React, { Component } from 'react';
import {
  Button, Card, CardBody, CardHeader, Modal,
  ModalBody, PaginationLink, Pagination, PaginationItem,
  Form, CustomInput,
  FormGroup,
  Label,
  Col, Row,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  ModalHeader, Alert, Table
} from 'reactstrap';
import { api } from '../../../views'
import qs from 'query-string'
import axios, { post } from 'axios';
import { connect } from "react-redux";
import ReactLoading from "react-loading";
import { Section, Title, Article, Prop, list } from "../../../controls/generic";


class Categories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false, loadding: false,
      name: '', image: '',
      description: '',
      categoryId: '',
      priority: 0,
      categories: '',
      imageError: '',
      nameError: '', tagError: '',
      descriptionError: '',
      tag: '', id: '', count: 0,
      prev: 0,
      next: 2,
      last: 0,
      first: 1,
      currentPage: 1,
      pagesNumber: [],
      isDone: false,
      imagePreviewUrl: '',
      imageName: '', imageType: ''
    };
    this.toggleSuccess = this.toggleSuccess.bind(this);
  }
  validate = () => {
    let nameError = "";
    let tagError = "";
    let imageError = "";
    let descriptionError = "";
    var { name, description, image, tag } = this.state;
    if (!name) {
      nameError = 'Trường tên không được để trống !'
    }
    // if (!tag) {
    //   tagError = 'Trường tag không được để trống !'
    // }
    // if (!description) {
    //   descriptionError = 'Trường mô tả không được để trống !'
    // }
    if (!image) {
      imageError = 'Bạn chưa chọn ảnh !'
    }
    if (nameError || imageError) {
      this.setState({ nameError, imageError });
      return false;
    }
    return true
  }
  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }
  // async  handleDeleteCategory(category) {
  //   if (confirm('Bạn chắc chắn muốn xóa ?')) { //eslint-disable-line
  //     api.dataService.actDeleteCategoryRequest(category.id)
  //     this.handleClick()
  //   }
  // }
  async handleClick(event) {
    let search = '?'
    let where = {};

    let name = qs.parse(this.props.location.search).name;
    let tag = qs.parse(this.props.location.search).tag;
    let id = qs.parse(this.props.location.search).id;
    if (name) {
      where.name = { 'contains': name }
      search += '&name=' + name
    }
    if (tag) {
      where.tag = { 'contains': tag }
      search += '&tag=' + tag
    }
    if (id) {
      where.id = { 'contains': id }
      search += '&id=' + id
    }
    if (event) {
      search += '&page=' + event;
    }
    await this.props.history.push(search)
    this.actFetchCategories(where)
  }
  componentDidMount() {
    this.handleClick()
  }

  async actFetchCategories(where) {
    let urlQueryObj = qs.parse(this.props.location.search);
    let currentPage = Number(urlQueryObj.page || 1);
    if (isNaN(currentPage) || currentPage < 1) {
      currentPage = 1;
    }
    let limit = urlQueryObj.limit || 10;
    let skip = (currentPage - 1) * limit;
    if (!where) {
      where = {}
    }
    var result = await api.dataService.actFetchCategoriesRequest(skip, limit, '', where, [{ id: 'DESC' }])
    let count = result.count;


    let maxPage = Math.ceil(count / limit);
    //console.log({ maxPage });
    //console.log({ currentPage });

    let pagesNumber = [];

    if (maxPage <= 5) {
      for (let i = 0; i < maxPage; i++) {
        pagesNumber.push(i + 1);
      }

    }
    else if (maxPage > 5 && maxPage - 2 <= currentPage) {
      pagesNumber = [0, maxPage - 4, maxPage - 3, maxPage - 2, maxPage - 1, maxPage];
    } else if (currentPage < 4) {
      pagesNumber = [1, 2, 3, 4, 5];
      if (maxPage > 5) {
        pagesNumber.push(0);
      }
    } else {
      pagesNumber = [0, currentPage - 2, currentPage - 1, currentPage, currentPage + 1, currentPage + 2, 0];
    }
    let next = currentPage + 1;

    let prev = currentPage - 1;

    this.setState({ next, prev, count, currentPage, last: maxPage || 1, pagesNumber });

  }

  onChange = async (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({
      [name]: value
    });
  }
  async  imageUpload(image, id) {
    console.log(image, 'jjjj')
    let token = localStorage.getItem('RFTK-GK') ? localStorage.getItem('RFTK-GK') : api.getToken()
    let url = api.config.HOST + '/api/file-management/upload-image'
    if (id) {
      url += '?id=' + id;
    }
    const formData = new FormData();
    formData.append('images', image)
    const config = {
      headers: {
        'Authorization': token,
        'content-type': 'multipart/form-data',
        'Api-Version': 1
      }
    }
    return await post(url, formData, config)
  }
  async  onChangeimage(e, id) {
    if (id) {
      await this.setState({ image: e.target.files[0], loadding: true, id: id }, () => {
        console.log('aaa', this.state.image)
      })
      this.imageUpload(this.state.image, id).then((response) => {
        console.log(response);
        if (response.status === 200) {
          this.setState({ image: response.data.created[0].id, loadding: false })
          window.location.reload(true);
        }
      })

    } else {
      let reader = new FileReader();
      let image = e.target.files[0];
      { image ? reader.readAsDataURL(image) : api.api.showMessage('Bạn chưa chọn ảnh', 'WARNING') }
      reader.onloadend = () => {
        this.setState({
          image: image,
          imagePreviewUrl: reader.result,
          imageName: image.name,
          imageType: image.type
        });
      }

    }
  }

  onSubmit = async e => {
    e.preventDefault();
    e.target.reset()
    var { name, description, tag, image,priority } = this.state;
    const isValid = this.validate()
    if (isValid) {
      this.imageUpload(this.state.image).then((response) => {
        console.log(response);
        if (response.status === 200 && response.data.created.length === 1) {
          // window.location.reload(true);
          var category = {
            name: name,
            description: description,
            tag: tag,
            image: response.data.created[0].id,
            priority
          }
          api.dataService.actAddCategoryRequest(category)
          this.handleClick()
          this.setState({
            success: !this.state.success,
            name: '',
            description: '',
            tag: '',
            priority:0
          })
        }
        else {
          api.api.showMessage(response.data.notCreate[0].error, 'Thông báo')
        }
      })


    }
  }
  onEnterPress = (e) => {
    if (e.keyCode == 13 && e.shiftKey == false) {
      e.preventDefault();
      this.onSubmitSearch();
    }
  }
  onSubmitSearch = async e => {
    e.preventDefault();
    e.target.reset()
    var { name, id, tag } = this.state;

    let where = {};
    if (id) {
      where.id = id
    }
    if (name) {
      where.name = { 'contains': name }
    }
    if (tag) {
      where.tag = { 'contains': tag }
    }
    api.dataService.actFetchCategoriesRequest(0, '', '', where, [{ name: 'ASC' }])
    this.setState({ currentPage: 1 })
    await this.props.history.push('?page=' + 1 + '&name=' + name + '&tag=' + tag + '&id=' + id)
    this.actFetchCategories(where)



  }
  render() {
    var { name, description, nameError, descriptionError, tag, tagError, id, currentPage, prev,
      next, pagesNumber, last, imageError, imageName, imageType, priority } = this.state;
    var { listCategories } = this.props;
    listCategories = this.props.listCategories.categories
    console.log('iii', listCategories)
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Bộ lọc
              </CardHeader>
              <CardBody>
                <Form onSubmit={this.onSubmitSearch}>
                  <Row form>
                    <Col md={4}>
                      <FormGroup>
                        <Label for="exampleCity">Mã danh mục</Label>
                        <Input type="text" id="id" name="id" value={id} onChange={this.onChange.bind(this)} />
                      </FormGroup>
                    </Col>
                    <Col md={4}>
                      <FormGroup>
                        <Label for="exampleState">Tên danh mục</Label>
                        <Input type="text" id="name" name="name" value={name} onChange={this.onChange.bind(this)} />
                      </FormGroup>
                    </Col>
                    <Col md={4}>
                      <FormGroup row>
                        <Label for="exampleZip">Tag </Label>
                        <Input type="text" id="tag" name="tag" value={tag} onChange={this.onChange.bind(this)} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <FormGroup className="form-actions">
                    <Button type="submit" size="sm" color="primary">Tìm kiếm</Button>
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>

          <Col xs={12} >
            <Button active color="success" onClick={this.toggleSuccess} className="float-right">Thêm danh mục</Button>
          </Col>
          <br />
          <br />
          <br />
          <Col xs={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> {listCategories.length > 0 ? `Danh mục truyện tranh (${listCategories.length} mục)` : 'Đang xử lý ...'}
              </CardHeader>
              <CardBody>
                {listCategories.length > 0 ?
                  <React.Fragment>
                    <Table responsive bordered>
                      <thead>
                        <tr>
                          <th className="text-center">#</th>
                          <th >Thông tin chung</th>
                          <th className="text-center" style={{ width: '300px' }}>Tag</th>
                          <th className="text-center">Hình ảnh</th>
                        </tr>
                      </thead>
                      <tbody>
                        {listCategories.map((category, index) => {
                          return (
                            <tr >
                              <th scope="row" className="text-center"><a href={`#/quanlysanpham/danhmuc/${category.id}`}>{category.id}</a></th>
                              <td >
                                <div className="small ">
                                  <span className="bold">Tên danh mục</span> | <a href={`#/quanlysanpham/danhmuc/${category.id}`}>{category.name}</a>
                                </div>
                                <div className="small ">
                                  <span className="bold">Mô tả</span> | <a href={`#/quanlysanpham/danhmuc/${category.id}`}>{(category.description || "").toString().substr(0, 100) + ((category.description || "").toString().length < 100 ? '' : ' ...')}</a>
                                </div>
                                <div className="small ">
                                  <span className="bold">Độ ưu tiên</span> | <a href={`#/quanlysanpham/danhmuc/${category.id}`}>{category.priority || 0}</a>
                                </div>
                                {/* <Button outline color="success" size="sm" className="mt-3" onClick={() => this.handleDeleteCategory(category)}>Xóa danh mục</Button> */}
                              </td>
                              <td >
                                <div className="small ">
                                  <span className="bold">Tag</span> |  <span >{category.tag}</span>
                                </div>
                              </td>
                              <td className="text-center" style={{ width: '250px' }}>
                                {this.state.loadding && this.state.id == category.image.id ?
                                  <Article style={{ display: 'table', margin: '0 auto' }}>
                                    <ReactLoading type="spin" color="#4acc7096" />
                                  </Article> : <img className="img-avatar-user" style={{ width: '160px', height: '160px' }} src={category.image.url} alt={category.image.serverFileDir} />}
                                <Label className="mt-2">Cập nhật ảnh</Label>
                                <CustomInput type="file" accept=".png,.svg,.jpg, .jpeg" id="image" name="image" size="sm" onChange={(data) => this.onChangeimage(data, category.image.id)} />

                              </td>
                            </tr >
                          )
                        }


                        )}
                      </tbody>
                    </Table> <div style={{ display: 'table', margin: '0 auto' }}>
                      {pagesNumber.length == 1 ? '' : <Pagination >
                        <PaginationItem>
                          {prev === 0 ? <PaginationLink className="hide">First</PaginationLink> :
                            <PaginationLink onClick={() => this.handleClick(1)} id="first">First</PaginationLink>
                          }
                        </PaginationItem>
                        <PaginationItem>
                          {prev < 1 ? <PaginationLink className="hide"><i className="cui-chevron-left icons "></i></PaginationLink> :
                            <PaginationLink onClick={() => this.handleClick(prev)} id="prev"><i className="cui-chevron-left icons "></i></PaginationLink>
                          }
                        </PaginationItem>
                        {!pagesNumber ? '' : pagesNumber.map((pageNumber, index) => {
                          return (<Pagination key={index}>
                            <PaginationItem active={currentPage == (pageNumber)} >
                              {pageNumber ? <PaginationLink onClick={() => { this.handleClick(pageNumber) }} key={index} >
                                {pageNumber}
                              </PaginationLink> : <PaginationLink className="disabled" onClick={() => { this.handleClick(pageNumber) }} key={index} >
                                  ...
                          </PaginationLink>
                              }
                            </PaginationItem>
                          </Pagination>)

                        })
                        }

                        <PaginationItem>
                          {
                            next > last ? <PaginationLink className="hide"><i className="cui-chevron-right icons "></i></PaginationLink> :
                              <PaginationLink onClick={() => this.handleClick(next)} id="next" > <i className="cui-chevron-right icons "></i></PaginationLink>
                          }
                        </PaginationItem>

                        <PaginationItem>
                          {
                            currentPage === last ? <PaginationLink className="hide">Last</PaginationLink> :
                              <PaginationLink onClick={() => this.handleClick(last)} id="last" >Last</PaginationLink>
                          }
                        </PaginationItem>
                      </Pagination>}
                    </div> </React.Fragment> : <Alert color="warning" className="center">
                    Không có dữ liệu !
      </Alert>}

              </CardBody>
            </Card>
          </Col>
          <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
            className={'modal-success ' + this.props.className}>
            <ModalHeader toggle={this.toggleSuccess}>Thêm danh mục truyện tranh</ModalHeader>
            <ModalBody>
              <Form onSubmit={this.onSubmit}>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '200px' }}>Tên danh mục</InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" id="name" name="name" value={name} onChange={this.onChange.bind(this)} />

                  </InputGroup>
                  {nameError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{nameError}</i> </p> : ''}
                </FormGroup>

                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '200px' }}>Mô tả</InputGroupText>
                    </InputGroupAddon>
                    <Input type="textarea" rows={4} id="description" name="description" value={description} onChange={this.onChange.bind(this)} />

                  </InputGroup>
                  {/* {descriptionError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{descriptionError}</i> </p> : ''} */}
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '200px' }}>Tag</InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" id="tag" name="tag" value={tag} onChange={this.onChange.bind(this)} />

                  </InputGroup>
                  {/* {tagError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{tagError}</i> </p> : ''} */}
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '120px' }}>Độ ưu tiên</InputGroupText>
                    </InputGroupAddon>
                    <Input type="number" min="0" id="priority" name="priority" value={priority} onChange={this.onChange.bind(this)} />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText style={{ width: '120px' }}>Hình ảnh</InputGroupText>
                    </InputGroupAddon>
                    {/* <Input type="text" id="image" name="image" value={image} onChange={this.onChange.bind(this)} /> */}
                    <CustomInput type="file" accept=".png,.svg,.jpg, .jpeg" id="image" name="image" onChange={this.onChangeimage.bind(this)} />

                  </InputGroup>
                  {imageError ? <p style={{ color: 'red', textAlign: 'center' }} className="mt-1"> <i>{imageError}</i> </p> : ''}
                </FormGroup>
                {this.state.imagePreviewUrl ? <div className="gallery-append" style={{ width: '100%' }} >
                  <img src={this.state.imagePreviewUrl} style={{ width: '100px', height: '100px' }} className="float-left" />
                  <div className="float-right"  >
                    <div className="small ">
                      <span className="bold">Name</span> | <span>{imageName}</span>
                    </div>
                    <div className="small ">
                      <span className="bold">Type</span> |<span>{imageType}</span>
                    </div>
                  </div>

                </div> : ''}

                <FormGroup className="form-actions text-right mt-2">
                  <Button type="submit" size="sm" color="primary" >Thêm danh mục</Button>
                </FormGroup>
              </Form>
            </ModalBody>
          </Modal>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    userReducer: state.userReducer,
    listCategories: state.listCategories,
  }
}
export default connect(mapStateToProps)(Categories);