import React, { Component } from 'react';
import {
  Alert, Card, CardBody, Container, Col, Row, Button,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from 'reactstrap';
import { api } from '../../../views'
import { connect } from "react-redux";

class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      nameError: '',
      descriptionError: '',
      tag: '',
      priority:0
    }
  }
  validate = () => {
    let nameError = "";
    let descriptionError = "";
    var { name, description } = this.state;
    if (!name) {
      nameError = 'Trường này không được để trống !'
    }
    if (!description) {
      descriptionError = 'Trường này không được để trống !'
    }
    if (nameError) {
      this.setState({ nameError });
      return false;
    }
    return true
  }

  onChange = async (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({
      [name]: value
    });
  }
  componentDidMount() {
    this.actGetCategory()
  }
  async actGetCategory() {
    var id = this.props.match.params.id
    // console.log('id', id)
    var result = await api.dataService.actFetchCategoriesRequest('', 10, '', { id: id }, '')
    // console.log('uuu', result)
    if (result.data.length === 1)
      this.setState({
        name: result.data[0].name,
        tag: result.data[0].tag,
        priority:result.data[0].priority,
        description: result.data[0].description,
      })
  }
  onSubmit = async e => {
    e.preventDefault();
    e.target.reset()
    var id = this.props.match.params.id
    var { description, name, tag ,priority} = this.state;
    const isValid = this.validate()
    if (isValid) {
      var category = {
        id: id,
        name: name,
        description: description,
        tag: tag,
        priority
      }
      await api.dataService.actUpdateCategoryRequest(category);
      api.dataService.actFetchCategoriesRequest('', 100, '')
      this.props.history.goBack(-1)
    }

  }
  render() {
    var { name, description, nameError, descriptionError, tag ,priority} = this.state;
    return (
      <Container>
        <Row className="justify-content-center animated fadeIn">
          <Col md="8">
            <Card className="mx-4">
              <CardBody className="p-4">
                <Alert color="warning" className="text-center">
                  Cập nhật
      </Alert>
                <Form onSubmit={this.onSubmit}>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '120px' }}>Tên danh mục</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="name" name="name" value={name} onChange={this.onChange.bind(this)} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {nameError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{nameError}</i> </p> : ''}
                  </FormGroup>

                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '120px' }}>Mô tả</InputGroupText>
                      </InputGroupAddon>
                      <Input type="textarea" rows={4} id="description" name="description" value={description} onChange={this.onChange.bind(this)} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {/* {descriptionError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{descriptionError}</i> </p> : ''} */}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '120px' }}>Tag</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="tag" name="tag" value={tag} onChange={this.onChange.bind(this)} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {/* {descriptionError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{descriptionError}</i> </p> : ''} */}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '120px' }}>Độ ưu tiên</InputGroupText>
                      </InputGroupAddon>
                      <Input type="number" min="0" id="priority" name="priority" value={priority} onChange={this.onChange.bind(this)} />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup className="form-actions text-right">
                    <Button type="submit" size="sm" color="primary">Lưu</Button>
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    userReducer: state.userReducer,
    listStores: state.listStores,
    itemEditing: state.itemEditing
  }
}
export default connect(mapStateToProps)(Category);

