function getHost() {
    let host =window.location.hostname;
    console.log({host})
    if(host==='dev.import.bookstoreaz.com'){
        return 'http://dev.backend.bookstoreaz.com'
    }
    if(host==='vi.import.bookstoreaz.com'){
        return 'http://vi.backend.bookstoreaz.com'
    }
    if(host==='en.import.bookstoreaz.com'){
        return 'http://en.backend.bookstoreaz.com'
    }
    return 'https://truyenonl.herokuapp.com';
}

const config = {
    // HOST: 'http://giatkydev.quangkhang.club',
    // HOST: 'http://backend.giatky.vn'
    // HOST: 'http://192.168.1.12:1337'
    // HOST: 'https://truyenonl.herokuapp.com'
    // HOST: 'http://dev.bookaz.quangkhang.club'
    HOST: getHost() || 'http://dev.backend.bookstoreaz.com'
}
export default config;