import { api, request } from '../../api';
import moment from 'moment'
import axios from 'axios';
const queryString = require('query-string');
var dataService = {
  login(account, password, type) {
    let url = 'api/auth/sign-in/local'
    let urlRFTK = 'api/auth/sign-in/refresh-token'
    let data
    if (type === 'local') {
      data = {
        type,
        account: account,
        password: password
      }
      return request.callApi(url, null, data, 'POST', null).then(res => {
        // console.log('xxx', res)
        if (res.status === 200) {
          return res
        }
        else {
          return (res,
            api.showMessage('Tài khoản người dùng không tồn tại ! Vui lòng thử lại', 'Thông báo'))

        }
      })
    }
    if (type === 'token') {
      data = {
        type,
      }
      return request.callApi(urlRFTK, null, data, 'POST', null)
    }
  },
  // logout() {
  //   let url = 'api/auth/logout'
  //   return request.callApi(url, 1, null, 'POST', null).then(res => {
  //     if (res.status === 200) {
  //       return res
  //     }
  //   })
  // },


  actFetchUsersRequest: (skip, limit, select, where, sort) => {
    let url = 'api/author'

    if (skip) {
      url += '?skip=' + (skip) + '&';
    }
    else {
      url += '?'
    }
    if (limit) {
      url += 'limit=' + (limit);
    }
    if (select) {
      url += '&select=' + (select);
    }
    if (where) {
      url += '&where=' + JSON.stringify(where);
    }
    if (sort) {
      url += '&sort=' + JSON.stringify(sort);
    }else{
      url += '&sort=' + JSON.stringify([{id:'DESC'}]);
    }
    url += '&populate=' + 'image,storyId';

    return request.callApi(url, 'public', null, 'GET', null).then(res => {
      if (res.status === 200) {
        api.actFetchUsers(res.data.data)
        return res.data
      }
      else {
        api.showMessage('Hệ thống gặp chút vấn đề ! Quý khách thông cảm !', 'Thông báo')
      }
    })
  },


  actFetchStoreUser: (skip, limit, select, where, sort) => {
    let url = 'api/storeUser'

    if (skip) {
      url += '?skip=' + (skip) + '&';
    }
    else {
      url += '?'
    }
    if (limit) {
      url += 'limit=' + (limit);
    }
    if (select) {
      url += '&select=' + (select);
    }
    if (where) {
      url += '&where=' + JSON.stringify(where);
    }
    if (sort) {
      url += '&sort=' + JSON.stringify(sort);
    }
    url += '&populate=' + 'userId,storeId';

    return request.callApi(url, 4, null, 'GET', null).then(res => {
      if (res.status === 200) {
        return res
      }
      else {
        api.showMessage('Hệ thống gặp chút vấn đề ! Wating...', 'Thông báo')
      }
    })
  },

  actGetRating: (skip, limit, to, froms, storeId) => {
    let url = 'api/report/rating'
    if (skip) {
      url += '?skip=' + (skip) + '&';
    }
    else {
      url += '?'
    }
    if (limit) {
      url += 'limit=' + (limit);
    }
    if (to) {
      url += '&to=' + moment(to).endOf('days').valueOf();
    } else {
      url += '&to=' + (moment().endOf('month'));
    }
    if (froms) {
      url += '&from=' + moment(froms).startOf('days').valueOf()
    } else {
      url += '&from=' + (moment().startOf('month'));
    }
    if (storeId && storeId !== 'undefined') {
      url += '&storeId=' + (storeId);
    }
    return request.callApi(url, 4, null, 'GET', null).then(res => {
      if (res.status === 200) {
        return res
      }
      else {
        api.showMessage('Hệ thống gặp chút vấn đề ! Wating...', 'Thông báo')
      }
    })
  },



  actFetchStoresRequest: (skip, limit, select, where) => {
    let url = 'api/user'

    if (skip) {
      url += '?skip=' + (skip) + '&';
    }
    else {
      url += '?'
    }
    if (limit) {
      url += 'limit=' + (limit);
    }
    if (select) {
      url += '&select=' + (select);
    }
    if (where) {
      url += '&where=' + JSON.stringify(where);
    }

    // url += '&populate=' + 'roleIds';

    return request.callApi(url, 3, null, 'GET', null).then(res => {
      console.log('ggg', res)
      if (res.status === 200) {
        api.actFetchStores(res.data.data)
        return res
      }
      else {
        api.showMessage('Hệ thống gặp chút vấn đề ! Wating...', 'Thông báo')
      }
    })
  },

  actFetchProductsRequest: (skip, limit, select, where, sort) => {
    let url = 'api/story'

    if (skip) {
      url += '?skip=' + (skip) + '&';
    }
    else {
      url += '?'
    }
    if (limit) {
      url += 'limit=' + (limit);
    }
    if (select) {
      url += '&select=' + (select);
    }

    if (where) {
      url += '&where=' + JSON.stringify(where);
    }
    if (sort) {
      url += '&sort=' + JSON.stringify(sort);
    }else{
      url += '&sort=' + JSON.stringify([{id:'DESC'}]);
    }
    url += '&populate=' + 'authorId,fileId,thumbnail,categoryId';
    return request.callApi(url, 'public', null, 'GET', null).then(res => {
      if (res.status === 200) {
        api.actFetchProducts(res.data.data)
        return res.data;
      }
      else {
        api.showMessage('Hệ thống gặp chút vấn đề ! Wating...', 'Thông báo')
      }
    })
  },

  actFetchCategoriesRequest: (skip, limit, select, where, sort, apiVersion) => {
    let url = 'api/category'

    if (skip) {
      url += '?skip=' + (skip) + '&';
    }
    else {
      url += '?'
    }
    if (limit) {
      url += 'limit=' + (limit);
    }
    if (select) {
      url += '&select=' + (select);
    }
    if (where) {
      url += '&where=' + JSON.stringify(where);
    }
    if (sort) {
      url += '&sort=' + JSON.stringify(sort);
    }else{
      url += '&sort=' + JSON.stringify([{id:'DESC'}]);
    }
    url += '&populate=' + 'image';
    return request.callApi(url, apiVersion || 'public', null, 'GET', null).then(res => {
      if (res.status === 200) {
        api.actFetchCategories(res.data.data)
        return res.data
      }
      else {
        api.showMessage('Hệ thống gặp chút vấn đề ! Wating...', 'Thông báo')
      }
    })
  },


  actFetchPriceProductsRequest: (skip, limit, select, where, sort) => {
    let url = 'api/storeAreaProduct'

    if (skip) {
      url += '?skip=' + (skip) + '&';
    }
    else {
      url += '?'
    }
    if (limit) {
      url += 'limit=' + (limit);
    }
    if (select) {
      url += '&select=' + (select);
    }
    if (where) {
      url += '&where=' + JSON.stringify(where);
    }
    if (sort) {
      url += '&sort=' + JSON.stringify(sort);
    }
    url += '&populate=' + 'productId,storeAreaId';
    return request.callApi(url, 1, null, 'GET', null).then(res => {
      console.log('ooo', res)
      if (res.status === 200) {
        api.actFetchPriceProducts(res.data.data)
        return res.data
      }
      else {
        api.showMessage('Hệ thống gặp chút vấn đề ! Wating...', 'Thông báo')
      }
    })
  },


  // actGetUserRequest: (id) => {
  //   let url = `api/user`
  //   return request.callApi(url, 1, id, 'GET', null).then(res => {
  //     console.log({ res })
  //     if (res.status === 200) {
  //       return api.actGetUser(res.data.data)
  //     }
  //     console.log(res.statusText)
  //     api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
  //   })
  // },



  // actGetStoreAreaRequest: (id) => {
  //   let url = `api/storeArea`
  //   return request.callApi(url, 1, id, 'GET', null).then(res => {
  //     console.log({ res })
  //     if (res.status === 200) {
  //       return api.actGetStoreArea(res.data.data)
  //     }
  //     console.log(res.statusText)
  //     api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
  //   })
  // },

  // actGetStoreRequest: (id) => {
  //   let url = `api/store`
  //   return request.callApi(url, 1, id, 'GET', null).then(res => {
  //     console.log({ res })
  //     if (res.status === 200) {
  //       return api.actGetStore(res.data.data)
  //     }
  //     console.log(res.statusText)
  //     api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
  //   })
  // },

  // actGetProductRequest: (id) => {
  //   let url = `api/product`
  //   return request.callApi(url, 1, id, 'GET', null).then(res => {
  //     console.log({ res })
  //     if (res.status === 200) {
  //       return api.actGetProduct(res.data.data)
  //     }
  //     console.log(res.statusText)
  //     api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
  //   })
  // },

  // actGetVoucherRequest: (id) => {
  //   let url = `api/voucher`
  //   return request.callApi(url, 5, id, 'GET', null).then(res => {
  //     console.log({ res })
  //     if (res.status === 200) {
  //       return api.actGetVoucher(res.data.data)
  //     }
  //     console.log(res.statusText)
  //     api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
  //   })
  // },

  // actGetCategoryRequest: (id) => {
  //   let url = `api/category`
  //   return request.callApi(url, 1, id, 'GET', null).then(res => {
  //     console.log({ res })
  //     if (res.status === 200) {
  //       return api.actGetCategory(res.data.data)
  //     }
  //     console.log(res.statusText)
  //     api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
  //   })
  // },

  actAddUserRequest: (user) => {
    let url = 'api/author'
    return request.callApi(url, 2, user, 'POST', null).then(res => {
      console.log({ res })
      if (res.status === 200) {

        return api.actAddUser(res.data)
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actAddStorePartnerRequest: (storepartner) => {
    let url = 'api/storePartner'
    return request.callApi(url, 5, storepartner, 'POST', null).then(res => {
      if (res.status === 200) {
        return api.actAddStorePartner(res.data)
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actAddRatingRequest: (rating) => {
    let url = 'api/rating'
    return request.callApi(url, 5, rating, 'POST', null).then(res => {
      if (res.status === 200) {
        return api.actAddRating(res.data)
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },


  actAddPriceProductRequest: (priceproduct) => {
    let url = 'api/storeAreaProduct'
    return request.callApi(url, 5, priceproduct, 'POST', null).then(res => {
      if (res.status === 200) {
        return api.actAddPriceProduct(res.data)
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actAddStoreRequest: (store) => {
    let url = 'api/user'
    return request.callApi(url, 3, store, 'POST', null).then(res => {
      if (res.status === 200) {
        return api.actAddStore(res.data)
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },
  actAddStoreAreaRequest: (storearea) => {
    let url = 'api/storeArea'
    return request.callApi(url, 5, storearea, 'POST', null).then(res => {
      if (res.status === 200) {
        return api.actAddStoreArea(res.data)
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actAddProductRequest: (product) => {
    let url = 'api/story'
    return request.callApi(url, 2, product, 'POST', null).then(res => {
      if (res.status === 200) {
        return api.actAddProduct(res.data)
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actAddCategoryRequest: (category) => {
    let url = 'api/category'
    return request.callApi(url, 2, category, 'POST', null).then(res => {
      if (res.status === 200) {
        return api.actAddCategory(res.data)
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actAddVoucherRequest: (voucher) => {
    let url = 'api/voucher'
    return request.callApi(url, 5, voucher, 'POST', null).then(res => {
      console.log('ttt', res)
      if (res.status === 200) {
        return api.actAddVoucher(res.data)
      }
      // console.log(res.statusText)
      // this.props.history.push('#/400')
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actAddStoreUserRequest: (storeuser) => {
    let url = 'api/store-user-management/add-user-to-store'
    return request.callApi(url, 4, storeuser, 'POST', null).then(res => {
      console.log('ttt', res)
      if (res.status === 200) {
        return res.data
      }
      // console.log(res.statusText)
      // this.props.history.push('#/400')
      api.showMessage("Người dùng hiện đã có trong danh sách cửa hàng !")
    })
  },

  actGetRole: (skip, limit, select, where) => {
    let url = 'api/role'

    if (skip) {
      url += '?skip=' + (skip) + '&';
    }

    else {
      url += '?'
    }
    if (limit) {
      url += 'limit=' + (limit);
    }
    if (select) {
      url += '&select=' + (select);
    }
    if (where) {
      url += '&where=' + JSON.stringify(where);
    }

    return request.callApi(url, 1, null, 'GET', null).then(res => {
      if (res.status === 200) {
        return res
      }
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actGetStorePartner: (skip, limit, select, where) => {
    // if (where) where = ();
    let url = 'api/storePartner'

    if (skip) {
      url += '&skip=' + (skip);
    }
    else {
      url += '?'
    }
    if (limit) {
      url += '&limit=' + (limit);
    }
    if (select) {
      url += '&select=' + (select);
    }
    if (where) {
      url += '&where=' + JSON.stringify(where);
    }
    // url += '&populate=' + 'avatar';
    console.log({ where })
    console.log({ url })
    return request.callApi(url, 1, null, 'GET', null).then(res => {
      console.log({ res })
      if (res.status === 200) {
        return res
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
    // return request.callApi(url, 'public', data, 'GET', null).then(res => res.json())
  },

  actUpdateUSerRequest: (user) => {
    console.log({ user })
    let url = `api/author/${user.id}`
    return request.callApi(url, 2, user, 'PATCH').then(res => {
      if (res.status === 200) {

        return api.actUpdateUser(res.data)
      }
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actUpdateUSerInfo: (user) => {
    console.log({ user })
    let url = `api/user/${user.id}`
    return request.callApi(url, 1, user, 'PATCH').then(res => {
      if (res.status === 200) {
        api.actUpdateUser(res.data)
        return res
      }
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actUpdateStorePartnerRequest: (storepartner) => {
    console.log({ storepartner })
    let url = `api/storePartner/${storepartner.id}`
    return request.callApi(url, 5, storepartner, 'PATCH').then(res => {
      if (res.status === 200) {

        return api.actUpdateStorePartner(res.data)
      }
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actUpdateStoreAreaRequest: (storearea) => {
    console.log({ storearea })
    let url = `api/storeArea/${storearea.id}`
    return request.callApi(url, 5, storearea, 'PATCH').then(res => {
      if (res.status === 200) {
        return api.actUpdateStoreArea(res.data)
      }
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actUpdateStoreRequest: (store) => {
    let url = `api/user/${store.id}`
    return request.callApi(url, 3, store, 'PATCH').then(res => {
      if (res.status === 200) {
        api.actUpdateStore(res.data)
        return res
      }
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actUpdateProductRequest: (product) => {
    let url = `api/story/${product.id}`
    return request.callApi(url, 2, product, 'PATCH').then(res => {
      if (res.status === 200) {
        return api.actUpdateProduct(res.data)
      }
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },


  actUpdateCategoryRequest: (category) => {
    console.log({ category })
    let url = `api/category/${category.id}`
    return request.callApi(url, 2, category, 'PATCH').then(res => {
      if (res.status === 200) {
        return api.actUpdateCategory(res.data)
      }
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actChangePassword: (password) => {
    let url = 'api/auth/change-password'
    return request.callApi(url, 1, password, 'PUT', null).then(res => {
      if (res.status === 200) {
        api.showMessage('Thay đổi mật khẩu thành công', 'Thông báo')
      }
      else {
        api.showMessage(res.data.message, 'Thông báo')
      }
    })
  },


  actResetPassword: (password) => {
    let url = 'api/auth/reset-password'
    return request.callApi(url, 3, password, 'PUT', null).then(res => {

      if (res.status === 200) {
        api.showMessage('Khôi phục mật khẩu thành công !', 'Thông báo')
        // return res

      }
      else {
        api.showMessage(res.data.message, 'Thông báo')
      }
    })
  },
  actBlockUser: (blockuser) => {
    let url = 'api/auth/user-management/block-user'
    return request.callApi(url, 1, blockuser, 'PUT', null).then(res => {
      if (res.status === 200) {
        // api.showMessage('Khôi phục mật khẩu thành công !', 'Thông báo')
        return res

      }
      else {
        api.showMessage('Hệ thông đang gặp lỗi ! Vui lòng thử lại trong vài phút !', 'Thông báo')
      }
    })
  },
  actUnBlockUser: (unblockuser) => {
    let url = 'api/auth/user-management/unblock-user'
    return request.callApi(url, 1, unblockuser, 'PUT', null).then(res => {
      if (res.status === 200) {
        // api.showMessage('Khôi phục mật khẩu thành công !', 'Thông báo')
        return res

      }

      api.showMessage('Hệ thông đang gặp lỗi ! Vui lòng thử lại trong vài phút !', 'Thông báo')
    })
  },
  actDeleteProductRequest: (id) => {
    let url = `api/story/${id}`
    return request.callApi(url, 2, null, 'DELETE').then(res => {
      if (res.status === 200) {
        return api.actDeleteProduct(id);
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actDeleteUserRequest: (id) => {
    let url = `api/author/${id}`
    return request.callApi(url, 2, null, 'DELETE').then(res => {
      if (res.status === 200) {
        return api.actDeleteUser(id);
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actDeleteCategoryRequest: (id) => {
    let url = `api/category/${id}`
    return request.callApi(url, 2, null, 'DELETE').then(res => {
      if (res.status === 200) {
        return api.actDeleteCategory(id);
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actDeletePriceProductRequest: (id) => {
    let url = `api/storeAreaProduct/${id}`
    return request.callApi(url, 5, null, 'DELETE').then(res => {
      if (res.status === 200) {
        return api.actDeletePriceProduct(id);
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actDeleteStoreUserRequest: (id) => {
    let url = `api/storeUser/${id}`
    return request.callApi(url, 4, null, 'DELETE').then(res => {
      if (res.status === 200) {
        return api.actDeleteStoreUser(id);
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },
  actDeleteRatingRequest: (id) => {
    let url = `api/rating/${id}`
    return request.callApi(url, 5, null, 'DELETE').then(res => {
      if (res.status === 200) {
        return api.actDeleteRating(id);
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },
  actDeleteStorePartnerRequest: (id) => {
    let url = `api/storePartner/${id}`
    return request.callApi(url, 5, null, 'DELETE').then(res => {
      if (res.status === 200) {
        return api.actDeleteStorePartner(id);
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },
  actRating: (rating) => {
    let url = 'api/bill-management/rating'
    return request.callApi(url, 'public', rating, 'POST', null).then(res => {
      if (res.status === 200) {
        return res
      }
    })
  },

  actPushNotice: (notice) => {
    let url = 'api/socket/push-notice'
    return request.callApi(url, 5, notice, 'POST', null).then(res => {
      if (res.status === 200) {
        return res
      }
      else {
        api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
      }
    })
  },
  actRegistration: (registration) => {
    let url = 'api/bill-management/registration-receive-at-home'
    return request.callApi(url, 'public', registration, 'POST', null).then(res => {
      if (res.status === 200) {
        return res
      }
    })
  },

  actGetStoreSpecificProducts: (skip, limit, select, where, sort) => {
    let url = 'api/StoreSpecificProduct'

    if (skip) {
      url += '&skip=' + (skip);
    }
    else {
      url += '?'
    }
    if (limit) {
      url += '&limit=' + (limit);
    }
    if (select) {
      url += '&select=' + (select);
    }
    if (where) {
      url += '&where=' + JSON.stringify(where);
    }
    if (sort) {
      url += '&sort=' + JSON.stringify(sort);
    }
    url += '&populate=' + 'storeId,productId,activeBy';
    return request.callApi(url, 5, null, 'GET', null).then(res => {
      if (res.status === 200) {
        return res
      }
      else {
        api.showMessage('Hệ thống gặp chút vấn đề ! Wating...', 'Thông báo')
      }
    })
  },

  actGetVKShareMonthlyStatistic: (skip, limit, select, where, sort) => {
    let url = 'api/VKShareMonthlyStatistic'

    if (skip) {
      url += '&skip=' + (skip);
    }
    else {
      url += '?'
    }
    if (limit) {
      url += '&limit=' + (limit);
    }
    if (select) {
      url += '&select=' + (select);
    }
    if (where) {
      url += '&where=' + JSON.stringify(where);
    }
    if (sort) {
      url += '&sort=' + JSON.stringify(sort);
    }
    url += '&populate=userId';
    return request.callApi(url, 5, null, 'GET', null).then(res => {
      if (res.status === 200) {
        return res
      }
      else {
        api.showMessage('Hệ thống gặp chút vấn đề ! Wating...', 'Thông báo')
      }
    })
  },

  actUpdateStoreSpecificProduct: (specific) => {
    let url = 'api/product-management/specific-store-price-action'
    return request.callApi(url, 5, specific, 'POST', null).then(res => {
      if (res.status === 200) {
        return res
      }
      // console.log(res.statusText)
      api.showMessage("Lỗi hệ thông ! Quý khách vui lòng quay lại trang chủ để chúng tôi lấy lại thông tin !")
    })
  },

  actGetReport: () => {
    let url = 'api/voucher-user-management/report-list-need-payment'
    return request.callApi(url, 5, null, 'GET', null).then(res => {
      if (res.status === 200) {
        return res
      }
      else {
        api.showMessage('Lỗi hệ thống! Xin thử lại sau !')
      }
    })
  }
}

export default dataService;
