import React, { Component } from 'react';
import { Button, Form, Card, CardBody, CardFooter, Container, FormGroup, InputGroupAddon, Col, InputGroup, Input, ModalFooter, InputGroupText, Row } from 'reactstrap';
import { api } from '../../../views'
class ChangePassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      passwordold: '',
      passwordnews: '',
      passwordnew: '',
      id: '',
      passwordoldError: '',
      passwordnewsError: '',
      passwordnewError: '',
    };


  }
  validate = () => {
    let passwordoldError = "";
    let passwordnewsError = "";
    let passwordnewError = "";
    var { passwordold, passwordnews, passwordnew } = this.state;
    if (!passwordnews) {
      passwordnewsError = 'Trường này không được để trống !'
    }
    if (!passwordold) {
      passwordoldError = 'Mật khẩu hiện tại này không được để trống  !'
    }
    if (!passwordnew) {
      passwordnewError = 'Mật khẩu mới không được để trống  !'
    }

    if (passwordoldError || passwordnewsError || passwordnewError) {
      this.setState({ passwordoldError, passwordnewsError, passwordnewError });
      return false;
    }
    return true
  }
  onSubmit = async e => {
    e.preventDefault();
    e.target.reset()
    var { username, passwordold, passwordnews, passwordnew, id } = this.state;
    const isValid = this.validate()
    if (isValid) {
      var password = {
        account: username,
        oldPassword: passwordold,
        newPassword: passwordnew,
      }
      if (passwordnews === passwordnew) {
        await api.dataService.actChangePassword(password)
      }
      else {
        api.api.showMessage('Mật khẩu không trùng khớp ! ', 'Thông báo')
      }
      this.setState({ passwordnewError: '', passwordnewsError: '', passwordoldError: '' })
    }
  }
  onChange = async (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({
      [name]: value
    });
  }
  render() {
    var { passwordnewError, passwordnewsError, passwordoldError } = this.state
    return (
      <Container>
        <Row className="justify-content-center animated fadeIn">
          <Col md="6">
            <Card className="mx-4">
              <CardBody className="p-4">
                <Form onSubmit={this.onSubmit}>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '170px' }}>Tài khoản</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="username" name="username" onChange={this.onChange.bind(this)} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '170px' }}>Mật khẩu hiện tại</InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" id="passwordold" name="passwordold" onChange={this.onChange.bind(this)} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-envelope"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {passwordoldError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{passwordoldError}</i> </p> : ''}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '170px' }}>Mật khẩu mới</InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" id="passwordnew" name="passwordnew" onChange={this.onChange.bind(this)} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-asterisk"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {passwordnewError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{passwordnewError}</i> </p> : ''}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '170px' }}>Nhập lại mật khẩu mới</InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" id="passwordnews" name="passwordnews" onChange={this.onChange.bind(this)} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-asterisk"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {passwordnewsError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{passwordnewsError}</i> </p> : ''}
                  </FormGroup>
                  <FormGroup className="form-actions text-center">
                    <Button type="submit" size="sm" color="primary">Thay đổi mật khẩu</Button>
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default ChangePassword;
