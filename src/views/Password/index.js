
import ChangePassword from './ChangePassword';
import ResetPassword from './ResetPassword';

export {
  ChangePassword, ResetPassword
};
