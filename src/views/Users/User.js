import React, { Component } from 'react';
import {
  Card, CardBody, Col, Row, Container, Button,
  Form, Alert,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from 'reactstrap';
import { api } from '../../views'
import { connect } from "react-redux";


class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      tag: '',
    }
  }

  onChange = async (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({
      [name]: value
    });
  }
  componentDidMount() {
    this.actGetUsers()
  }

  async actGetUsers() {
    var id = this.props.match.params.id
    var result = await api.dataService.actFetchUsersRequest('', 10, '', { id: id }, '')
    console.log('uuu', result)
    if (result.data.length === 1)
      this.setState({
        name: result.data[0].name,
        description: result.data[0].description,
        image: result.data[0].image.url,
        tag: result.data[0].tag,
      })
  }

  onEnterPress = (e) => {
    if (e.keyCode == 13 && e.shiftKey == false) {
      e.preventDefault();
      this.onSubmit();
    }
  }
  onSubmit = async e => {
    e.preventDefault();
    e.target.reset()
    var id = this.props.match.params.id
    var { name, image, tag, description } = this.state;
    var user = {
      id: id,
      name: name,
      tag: tag || undefined,
      // image: image || undefined,
      description: description || undefined,
    }
    await api.dataService.actUpdateUSerRequest(user);
    this.props.history.goBack(-1)
  }
  render() {
    var { name, image, tag, description } = this.state;
    return (
      <Container>
        <Row className="justify-content-center animated fadeIn">
          <Col md="8">
            <Card className="mx-4">
              <CardBody className="p-4">
                <Alert color="warning" className="text-center">
                  Cập nhật tác giả
      </Alert>
                <Form onSubmit={this.onSubmit}>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '120px' }}>Tên tác giả</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="name" name="name" value={name} onChange={this.onChange.bind(this)} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '120px' }}>Mô tả</InputGroupText>
                      </InputGroupAddon>
                      <Input type="textarea" rows={4}  id="description" name="description" value={description} onChange={this.onChange.bind(this)} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '120px' }}>Tag</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="tag" name="tag" value={tag} onChange={this.onChange.bind(this)} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-envelope"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>

                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText style={{ width: '120px' }}>Hình ảnh</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="image" name="image" value={image} onChange={this.onChange.bind(this)} disabled />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                  <FormGroup className="form-actions text-right">
                    <Button type="submit" size="sm" color="primary">Lưu</Button>
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    userReducer: state.userReducer,
    listUsers: state.listUsers,
    itemEditing: state.itemEditing
  }
}
export default connect(mapStateToProps)(User);

