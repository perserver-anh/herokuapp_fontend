import React, { Component } from 'react';
import {
  Badge, Card, CardBody, CardHeader, Col, Row, Table, Button,
  Modal,
  ModalBody, CustomInput,
  Form,
  FormGroup,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  ModalHeader, Pagination, PaginationItem, PaginationLink
} from 'reactstrap';
import { AppSwitch } from '@coreui/react'
import { api } from '../../views'
import axios, { post } from 'axios';
import { connect } from "react-redux";
import qs from 'query-string'
import '../../index.css'
import ReactLoading from "react-loading";
import { Section, Title, Article, Prop, list } from "../../controls/generic";

class ListUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
      loadding: false,
      name: '',
      nameError: '',
      tag: '',
      tagError: '',
      image: '',
      imageError: '',
      description: '',
      descriptionError: '',
      id: '',
      users: [],
      count: 0,
      prev: 0,
      next: 2,
      last: 0,
      first: 1,
      currentPage: 1,
      pagesNumber: [],
      name: '', isDone: false, imagePreviewUrl: '',
      imageName: '', imageType: ''
    };
    this.toggleSuccess = this.toggleSuccess.bind(this);
  }
  async handleClick(event) {
    let search = '?'
    let where = {};

    let name = qs.parse(this.props.location.search).name;
    if (name) {
      where.name = { 'contains': name }
      search += '&name=' + name
    }
    if (event) {
      search += '&page=' + event;
    }
    await this.props.history.push(search)
    this.actFetchUsers(where)
  }
  validate = () => {
    let tagError = "";
    let descriptionError = "";
    let imageError = "";
    let fullnameError = "";
    let nameError = "";
    var { name, fullname, tag, description, image } = this.state;
    if (!name) {
      nameError = 'Trường này không được để trống !'
    }
    if (!fullname) {
      fullnameError = 'Trường này không được để trống !'
    }
    if (!tag) {
      tagError = 'Trường này không được để trống !'
    }
    if (!description) {
      descriptionError = 'Trường này không được để trống !'
    }
    if (!image) {
      imageError = 'Trường này không được để trống !'
    }

    if (tagError || nameError || fullnameError || descriptionError || imageError) {
      this.setState({ tagError, nameError, fullnameError, descriptionError, imageError });
      return false;
    }
    return true
  }
  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }

  componentDidMount() {
    this.handleClick()
  }

  async actFetchUsers(where) {
    let urlQueryObj = qs.parse(this.props.location.search);
    let currentPage = Number(urlQueryObj.page || 1);
    if (isNaN(currentPage) || currentPage < 1) {
      currentPage = 1;
    }
    let limit = urlQueryObj.limit || 10;
    let skip = (currentPage - 1) * limit;
    if (!where) {
      where = {}
    }
    var result = await api.dataService.actFetchUsersRequest(skip, limit, '', where, [{id:'DESC'}])
    let count = result.count;


    let maxPage = Math.ceil(count / limit);
    //console.log({ maxPage });
    //console.log({ currentPage });

    let pagesNumber = [];

    if (maxPage <= 5) {
      for (let i = 0; i < maxPage; i++) {
        pagesNumber.push(i + 1);
      }

    }
    else if (maxPage > 5 && maxPage - 2 <= currentPage) {
      pagesNumber = [0, maxPage - 4, maxPage - 3, maxPage - 2, maxPage - 1, maxPage];
    } else if (currentPage < 4) {
      pagesNumber = [1, 2, 3, 4, 5];
      if (maxPage > 5) {
        pagesNumber.push(0);
      }
    } else {
      pagesNumber = [0, currentPage - 2, currentPage - 1, currentPage, currentPage + 1, currentPage + 2, 0];
    }
    let next = currentPage + 1;

    let prev = currentPage - 1;

    this.setState({ next, prev, count, currentPage, last: maxPage || 1, pagesNumber });

  }
  // async  deleteAuthor(user) {
  //   if (confirm('Bạn chắc chắn muốn xóa ?')) { //eslint-disable-line
  //     api.dataService.actDeleteUserRequest(user.id)
  //     this.actFetchUsers()
  //   }

  // }

  async  imageUpload(image, id) {
    console.log(image, 'jjjj')
    let token = localStorage.getItem('RFTK-GK') ? localStorage.getItem('RFTK-GK') : api.getToken()
    let url = api.config.HOST + '/api/file-management/upload-image'
    if (id) {
      url += '?id=' + id;
    }
    const formData = new FormData();
    formData.append('images', image)
    const config = {
      headers: {
        'Authorization': token,
        'content-type': 'multipart/form-data',
        'Api-Version': 1
      }
    }
    return await post(url, formData, config)
  }

  async  onChangeimage(e, id) {
    if (id) {
      await this.setState({ image: e.target.files[0], loadding: true, id: id }, () => {
        console.log('aaa', this.state.image)
      })

      this.imageUpload(this.state.image, id).then((response) => {
        console.log(response);
        if (response.status === 200) {
          this.setState({ image: response.data.created[0].id, loadding: false })
          window.location.reload(true);
        }

      })

    } else {
      let reader = new FileReader();
      let image = e.target.files[0];
      { image ? reader.readAsDataURL(image) : api.api.showMessage('Bạn chưa chọn ảnh', 'WARNING') }
      reader.onloadend = () => {
        this.setState({
          image: image,
          imagePreviewUrl: reader.result,
          imageName: image.name,
          imageType: image.type
        });
      }

    }

  }
  onSubmit = async e => {
    e.preventDefault();
    e.target.reset()
    const isValid = this.validate()
    var { name, image, tag, description } = this.state;

    if (!isValid) {
      this.imageUpload(this.state.image).then((response) => {
        console.log(response);
        if (response.status === 200 && response.data.created.length === 1) {
          var user = {
            name: name,
            tag: tag || undefined,
            image: response.data.created[0].id,
            description: description || undefined,
          }
          api.dataService.actAddUserRequest(user)
          this.handleClick()
          this.setState({
            success: !this.state.success,
            name: '',
            tag: '',
            description: '',
            image: ''
          })
        }
        else {
          api.api.showMessage(response.data.notCreate[0].error, 'Thông báo')
        }
      })
    }
  }
  onChange = async (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    await this.setState({
      [name]: value
    });
  }
  onEnterPress = (e) => {
    if (e.keyCode == 13 && e.shiftKey == false) {
      e.preventDefault();
      this.onSubmitSearch();
    }
  }
  onSubmitSearch = async e => {
    e.preventDefault();
    e.target.reset()
    var { name } = this.state;
    let where = {};

    if (name) {
      where.name = { 'contains': name }
    }

    this.setState({ currentPage: 1 })
    await this.props.history.push('?page=' + 1 + '&name=' + name)
    this.actFetchUsers(where)

  }
  render() {
    var { name, image, tag, tagError, currentPage, prev, name,
      next, pagesNumber, last, descriptionError,
      imageError, nameError, description, imageName, imageType
    } = this.state;
    var { listAuthor } = this.props;
    listAuthor = this.props.listAuthor.users

    // const userList = usersData.filter((user) => user.data.id < 10)

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Bộ lọc
              </CardHeader>
              <CardBody>
                <Form onSubmit={this.onSubmitSearch}>
                  <Row form>
                    {/* <Col md={4}>
                      <FormGroup>
                        <Label for="exampleCity">Mã</Label>
                        <Input type="text" id="id" name="id" value={id} onChange={this.onChange.bind(this)} />
                      </FormGroup>
                    </Col> */}
                    <Col md={4}>
                      <FormGroup>
                        <Label for="exampleState">Tên tác giả</Label>
                        <Input type="text" id="name" name="name" value={name} onChange={this.onChange.bind(this)} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <FormGroup className="form-actions">
                    <Button type="submit" size="sm" color="primary">Tìm kiếm</Button>
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>
          <Col xs={12} >
            <Button active color="success" onClick={this.toggleSuccess} className="float-right">Thêm mới tác giả</Button>
          </Col>
          <br />
          <br />
          <br />
          <Col xl={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> {listAuthor.length > 0 ? `Dữ liệu (${listAuthor.length} mục)` : 'Đang xử lý ...'}
              </CardHeader>
              <CardBody>
                <Table responsive bordered hover>
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Thông tin tác giả</th>
                      <th scope="col">Tag</th>
                      <th scope="col" className="text-center">Số lượng người tải</th>
                      <th scope="col" className="text-center">Ảnh nền</th>
                    </tr>
                  </thead>
                  <tbody>
                    {listAuthor.length > 0 ? listAuthor.map((user, index) =>
                      <tr >
                        <th scope="row"><a href={`#/users/${user.id}`}>{user.id}</a></th>
                        <td> <div className="small ">
                          <span className="bold">Tên</span> | {user.name}
                        </div>
                          <div className="small ">
                            <span className="bold">Mô tả</span> | {(user.description || "").toString().substr(0, 100) + ((user.description || "").toString().length < 100 ? '' : ' ...')}
                          </div>
                          <Button color="link" href={`#/users/${user.id}`} >Xem chi tiết</Button>
                          {/* <Button color="link" onClick={() => this.deleteAuthor(user)} >Xóa</Button> */}

                        </td>
                        <td>{user.tag}

                        </td>
                        <td className="text-center">
                          <span style={{ fontSize: 24 + 'px' }} className="bold mt-4">{user.countDownload}</span>
                        </td>
                        <td className="text-center" style={{ width: '250px' }}>
                          {this.state.loadding && this.state.id == user.image.id ?
                            <Article style={{ display: 'table', margin: '0 auto' }}>
                              <ReactLoading type="spin" color="#4acc7096" />
                            </Article> : <img className="img-avatar-user" style={{ width: '150px', height: '150px' }} src={user.image.url} alt={user.image.serverFileDir} />}
                          <Label className="mt-2">Cập nhật ảnh</Label>
                          {/* <Button color="link" outline outline className=" mt-2" size="sm" onClick={() => { imageUpload(product.thumbnail.id) }}>Cập nhật ảnh</Button> */}
                          <CustomInput type="file" accept=".png,.svg,.jpg, .jpeg" id="thumbnail" name="thumbnail" size="sm" onChange={(data) => this.onChangeimage(data, user.image.id)} />
                        </td>
                      </tr >
                    ) : <span>Không có dữ liệu !</span>}
                  </tbody>
                </Table>
                <div style={{ display: 'table', margin: '0 auto' }}>
                  {pagesNumber.length == 1 ? '' : <Pagination >
                    <PaginationItem>
                      {prev === 0 ? <PaginationLink className="hide">First</PaginationLink> :
                        <PaginationLink onClick={() => this.handleClick(1)} id="first">First</PaginationLink>
                      }
                    </PaginationItem>
                    <PaginationItem>
                      {prev < 1 ? <PaginationLink className="hide"><i className="cui-chevron-left icons "></i></PaginationLink> :
                        <PaginationLink onClick={() => this.handleClick(prev)} id="prev"><i className="cui-chevron-left icons "></i></PaginationLink>
                      }
                    </PaginationItem>
                    {!pagesNumber ? '' : pagesNumber.map((pageNumber, index) => {
                      return (<Pagination key={index}>
                        <PaginationItem active={currentPage == (pageNumber)} >
                          {pageNumber ? <PaginationLink onClick={() => { this.handleClick(pageNumber) }} key={index} >
                            {pageNumber}
                          </PaginationLink> : <PaginationLink className="disabled" onClick={() => { this.handleClick(pageNumber) }} key={index} >
                              ...
                          </PaginationLink>
                          }
                        </PaginationItem>
                      </Pagination>)

                    })
                    }

                    <PaginationItem>
                      {
                        next > last ? <PaginationLink className="hide"><i className="cui-chevron-right icons "></i></PaginationLink> :
                          <PaginationLink onClick={() => this.handleClick(next)} id="next" > <i className="cui-chevron-right icons "></i></PaginationLink>
                      }
                    </PaginationItem>

                    <PaginationItem>
                      {
                        currentPage === last ? <PaginationLink className="hide">Last</PaginationLink> :
                          <PaginationLink onClick={() => this.handleClick(last)} id="last" >Last</PaginationLink>
                      }
                    </PaginationItem>
                  </Pagination>}
                </div>
              </CardBody>
            </Card>
          </Col>
          <br />
          <br />
        </Row>
        <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
          className={'modal-success ' + this.props.className}>
          <ModalHeader toggle={this.toggleSuccess}>Thêm mới tác giả</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '120px' }}>Tên tác giả</InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" id="name" name="name" value={name} onChange={this.onChange.bind(this)} />

                </InputGroup>
                {nameError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{nameError}</i> </p> : ''}
              </FormGroup>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '120px' }}>Mô tả</InputGroupText>
                  </InputGroupAddon>
                  <Input type="textarea" rows={4} id="description" name="description" value={description} onChange={this.onChange.bind(this)} />

                </InputGroup>
                {descriptionError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{descriptionError}</i> </p> : ''}
              </FormGroup>
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '120px' }}>Thể loại</InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" id="tag" name="tag" value={tag} onChange={this.onChange.bind(this)} />

                </InputGroup>
                {tagError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{tagError}</i> </p> : ''}
              </FormGroup>

              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText style={{ width: '120px' }}>Hình ảnh</InputGroupText>
                  </InputGroupAddon>
                  {/* <Input type="text" id="image" name="image" value={image} onChange={this.onChange.bind(this)} /> */}
                  <CustomInput type="file" accept=".jpg, .jpeg" id="image" name="image" onChange={this.onChangeimage.bind(this)} />

                </InputGroup>
                {imageError ? <p style={{ color: 'red', textAlign: 'center' }}> <i>{imageError}</i> </p> : ''}
              </FormGroup>
              {this.state.imagePreviewUrl ? <div className="gallery-append" style={{ width: '100%' }} >
                <img src={this.state.imagePreviewUrl} style={{ width: '100px', height: '100px' }} className="float-left" />
                <div className="float-right"  >
                  <div className="small ">
                    <span className="bold">Name</span> | <span>{imageName}</span>
                  </div>
                  <div className="small ">
                    <span className="bold">Type</span> |<span>{imageType}</span>
                  </div>
                </div>

              </div> : ''}
              <FormGroup className="form-actions text-right mt-2">
                <Button type="submit" size="sm" color="primary" >Thêm</Button>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    userReducer: state.userReducer,
    listAuthor: state.listAuthor,
  }
}
export default connect(mapStateToProps)(ListUsers);

