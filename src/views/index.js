import { PersonalInformation } from './ThongTinNguoiDung'
import { Storis, Categories } from './QuanLySanPham';
import Dashboard from './Dashboard';
import { Login, Page404, Page500 } from './Pages';
import * as api from './api/index'
import { ChangePassword, ResetPassword } from './Password'
import { Accounts } from './QuanLyTaiKhoan'
export {
  Accounts,
  PersonalInformation,
  api,
  ResetPassword,
  ChangePassword,
  Page404,
  Page500,
  Login,
  Storis,
  Categories,
  Dashboard,
};

