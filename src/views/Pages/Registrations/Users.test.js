import React from 'react';
import ReactDOM from 'react-dom';
import Registrations from './Registrations';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Registrations />, div);
  ReactDOM.unmountComponentAtNode(div);
});
