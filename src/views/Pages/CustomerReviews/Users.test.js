import React from 'react';
import ReactDOM from 'react-dom';
import CustomerReviews from './CustomerReviews';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CustomerReviews />, div);
  ReactDOM.unmountComponentAtNode(div);
});
