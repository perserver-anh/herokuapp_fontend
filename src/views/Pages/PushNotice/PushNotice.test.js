import React from 'react';
import ReactDOM from 'react-dom';
import PushNotice from './PushNotice';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PushNotice />, div);
  ReactDOM.unmountComponentAtNode(div);
});
