
export default {
  items: [

    {
      title: true,
      name: 'ADMIN',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Quản lý truyện tranh ',
      url: '/quanlysanpham',
      icon: 'cui-globe',
      children: [
        {
          name: 'Danh mục truyện',
          url: '/quanlysanpham/danhmuc',
          icon: 'cui-brush',
        },
        {
          name: 'Danh sách truyện',
          url: '/quanlysanpham/danhsach',
          icon: 'cui-brush',
        },
      ],
    },
    {
      name: 'Quản lý tài khoản',
      url: '/quanlytaikhoan',
      icon: 'icon-basket-loaded',
      children: [
        {
          name: 'Danh sách người dùng',
          url: '/quanlytaikhoan/danhsach',
          icon: 'cui-social-spotify',

        },
      ]
    },

    {
      name: 'Thay đổi mật khẩu',
      url: '/password',
      icon: 'cui-settings',
      children: [

        {
          name: 'Mật khẩu người dùng',
          url: '/password/changepassword',
          icon: 'cui-speedometer',
        },
        {
          name: 'Khôi phục mật khẩu',
          url: '/password/resetpassword',
          icon: 'cui-people',
        },

      ],
    },
    // {
    //   name: 'Widgets',
    //   url: '/widgets',
    //   icon: 'icon-calculator',
    //   badge: {
    //     variant: 'info',
    //     text: 'NEW',
    //   },
    // },
    {
      divider: true,
    },
    // {
    //   title: true,
    //   name: 'ĐĂNG NHẬP VÀ BẢO MẬT',
    // },
    // {
    //   name: 'ĐĂNG NHẬP',
    //   url: '/pages',
    //   icon: 'cui-user',
    //   children: [
    //     {
    //       name: 'Login',
    //       url: '/login',
    //       icon: 'cui-user',
    //     },

    //   ],
    // },

    // {
    //   name: 'Download CoreUI',
    //   url: 'http://coreui.io/react/',
    //   icon: 'icon-cloud-download',
    //   class: 'mt-auto',
    //   variant: 'success',
    // },
    // {
    //   name: 'Try CoreUI PRO',
    //   url: 'http://coreui.io/pro/react/',
    //   icon: 'icon-layers',
    //   variant: 'danger',
    // },
  ],
};
