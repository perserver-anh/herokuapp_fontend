// import langReducer from "./langReducer";
import uiReducer from "./uiReducer";
import userReducer from './userReducers';
// import cateState from './categoryReducer';
import { combineReducers } from "redux";
import listAuthor from './listAuthor'
import itemEditing from './itemEditing'
import listStores from './listStores'
import listProducts from './listProducts'
import listCategories from './listCategories'
// import search from './search'
// import filterTable from './filterTable'
// import sort from './sort'

const rootReducer = combineReducers({
    listCategories, listProducts,
    listStores, uiReducer, userReducer, listAuthor, itemEditing
});

export default rootReducer;
