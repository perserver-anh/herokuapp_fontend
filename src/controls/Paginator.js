import React, { Component } from 'react';
import {
    PaginationItem,
    PaginationLink,
    Pagination
} from 'reactstrap';
import qs from 'query-string'
import { api } from '../views'
class Paginator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prev: this.props.prev,
            next: this.props.next,
            last: this.props.last,
            currentPage: this.props.currentPage,
            pagesNumber: this.props.pagesNumber,
        };
    }

    render() {
        var { currentPage, prev, next, pagesNumber, last } = this.state;
        var { handleClick } = this.props;
        return (
            <div style={{ display: 'table', margin: '0 auto' }}>
                {pagesNumber.length == 1 ? '' : <Pagination >
                    <PaginationItem>
                        {prev === 0 ? <PaginationLink className="hide">First</PaginationLink> :
                            <PaginationLink onClick={() => handleClick(1)} id="first">First</PaginationLink>
                        }
                    </PaginationItem>
                    <PaginationItem>
                        {prev < 1 ? <PaginationLink className="hide"><i className="cui-chevron-left icons "></i></PaginationLink> :
                            <PaginationLink onClick={() => handleClick(prev)} id="prev"><i className="cui-chevron-left icons "></i></PaginationLink>
                        }
                    </PaginationItem>
                    {!pagesNumber ? '' : pagesNumber.map((pageNumber, index) => {
                        return (<Pagination key={index}>
                            <PaginationItem active={currentPage == (pageNumber)} >
                                {pageNumber ? <PaginationLink onClick={() => { handleClick(pageNumber) }} key={index} >
                                    {pageNumber}
                                </PaginationLink> : <PaginationLink className="disabled" onClick={() => { handleClick(pageNumber) }} key={index} >
                                        ...
                          </PaginationLink>
                                }
                            </PaginationItem>
                        </Pagination>)

                    })
                    }

                    <PaginationItem>
                        {
                            next > last ? <PaginationLink className="hide"><i className="cui-chevron-right icons "></i></PaginationLink> :
                                <PaginationLink onClick={() => handleClick(next)} id="next" > <i className="cui-chevron-right icons "></i></PaginationLink>
                        }
                    </PaginationItem>

                    <PaginationItem>
                        {
                            currentPage === last ? <PaginationLink className="hide">Last</PaginationLink> :
                                <PaginationLink onClick={() => handleClick(last)} id="last" >Last</PaginationLink>
                        }
                    </PaginationItem>
                </Pagination>}

            </div>
        )
    }
}

export default Paginator;

// let fromIndex = (this.props.currentPage - 1) * this.props.itemPerPage + 1;
// let toIndex = fromIndex + Number(this.props.itemPerPage) - 1;
// if (toIndex > this.props.count) toIndex = this.props.count;

// let options = [10, 20, 30];
// let pages = [];
// for (var i = 0; i < this.props.count / this.props.itemPerPage; i++) {
//     pages.push(i + 1);
// }

{/* <div className='paginator'>
            <span>{fromIndex} đến {toIndex} trong tổng số {this.props.count}. Hiển thị</span>
            <Input type="select" onChange={evt => {
                if (this.props.onItemPerPageChanged) {
                    this.props.onItemPerPageChanged(evt.target.value);
                }
            }}>
                {options.map(o => <option value={o} key={o}>{o}</option>)}
            </Input>
            <span>bản ghi mỗi trang. Đi tới trang</span>
            <Input type="select" onChange={evt => {
                if (this.props.onPageChanged) {
                    this.props.onPageChanged(evt.target.value);
                }
            }}>
                {pages.map(p => <option value={p} key={p}>{p}</option>)}
            </Input>
        </div> */}