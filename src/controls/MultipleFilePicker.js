import React, { Component } from 'react';
import {
    FormGroup,
    Input,
    Label,
    Button,
    Row,
    Col
} from 'reactstrap';
import request from '../services/request';
import FilePicker from './FilePicker';
class MultipleFilePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.value
        }
    }
    async uploadFile(file) {
        let formData = new FormData();
        formData.append('file', file);
        let w = this.props.width || 300;
        let h = this.props.height || 300;
        let rs = await request.upload('/image/upload?folder=deal&w=560&h=293', formData);
        if (this.props.onChange) {
            this.props.onChange(rs.url);
        }
    }
    onAddClick() {
        if (!this.data) this.data = [];
        this.data.push('');
        if (this.props.onChange) {
            this.props.onChange(this.data);
        }
    }
    onRemoveClick(k) {
        if (!this.data) return;
        this.data.splice(k, 1);
        if (this.props.onChange) {
            this.props.onChange(this.data);
        }
    }
    data = [];
    render() {
        this.data = this.props.value;
        let addButton = () => (<Col sm='4'><Button color="default" type='button' onClick={this.onAddClick.bind(this)} className='btn-brand'><i className="fa fa-plus"></i><span>Thêm tệp</span></Button></Col>)
        if (!this.data) return addButton();
        return (
            <Row>
                {this.data.map((v, k) => {
                    return (<Col sm='4' key={k}>
                        <div className='file-picker-container'>
                            <FilePicker value={v} onChange={url => {
                                if (this.props.onChange) {
                                    this.data[k] = url;
                                    this.props.onChange(this.data);
                                }
                            }} />
                            <div className='multiple-remove-button'>
                                <Button color="danger" type='button' onClick={() => {
                                    this.onRemoveClick(k);
                                }}><i className="fa fa-remove"></i></Button>
                            </div></div>
                    </Col>)
                })}
                {addButton()}
            </Row>
        )
    }
}

export default MultipleFilePicker;
