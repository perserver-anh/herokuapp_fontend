import React, { Component } from 'react';
import {
    FormGroup,
    Input,
    Label
} from 'reactstrap';
import DatePickerComponent from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
class DatePicker extends Component {
    constructor(props) {
        super(props);
        let data = null;
        if (props.value) {
            data = moment(props.value);
        }
        this.state = {
            startDate: data
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(date) {
        this.setState({
            startDate: date
        });
        if (this.props.onChange) {
            this.props.onChange(date.toDate());
        }
    }

    render() {
        // return null;
        return (<DatePickerComponent
            selected={this.state.startDate}
            onChange={this.handleChange}
            className="form-control full-width"
            dateFormat='DD/MM/YYYY'
            locale="vi-gb"
        />);
    }
}

export default DatePicker;
