import React, { Component } from 'react';
import {
    FormGroup,
    Input,
    Label,
    Badge
} from 'reactstrap';
class Checkbox extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let activeText = this.props.activeText ? this.props.activeText : 'Bật';
        let inactiveText = this.props.inactiveText ? this.props.inactiveText : 'Tắt';
        return (<FormGroup check inline>
            <Label check>
                <Input type="checkbox" defaultChecked={this.props.value} onChange={evt => {
                    if (this.props.onChange) {
                        this.props.onChange(evt.target.checked);
                    }
                }} />
                {this.props.value ? (<Badge color="success">{activeText}</Badge>) : (<Badge color="default">{inactiveText}</Badge>)}
            </Label>
        </FormGroup>)
    }
}

export default Checkbox;
