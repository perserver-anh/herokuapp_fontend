import React, { Component } from 'react';
import {
    Table,
    Input,
    Label,
    Badge
} from 'reactstrap';
class TableData extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        // let activeText = this.props.activeText ? this.props.activeText : 'Bật';
        // let inactiveText = this.props.inactiveText ? this.props.inactiveText : 'Tắt';
        return (
            <Table hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                </tbody>
            </Table>
        )
    }
}

export default TableData;
