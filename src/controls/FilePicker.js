import React, { Component } from 'react';
import {
    FormGroup,
    Input,
    Label
} from 'reactstrap';
import request from '../services/request';
class FilePicker extends Component {
    async uploadFile(file) {
        let formData = new FormData();
        formData.append('file', file);
        let rs = await request.upload('/image/upload?folder=deal&w=560&h=293', formData);
        if (this.props.onChange) {
            this.props.onChange(rs.url);
        }
    }
    render() {
        return (<div>
            {/* hiển thị ảnh thumbnail */}
            {this.props.value ? (
                <img src={this.props.value} className='file-picker-thumbnail' />
            ) : null}
            <Input type="file" onChange={evt => {
                this.uploadFile(evt.target.files[0]);
            }} />
        </div>)
    }
}

export default FilePicker;
