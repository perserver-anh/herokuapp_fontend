import React, { Component } from 'react';
import {
    FormGroup,
    Input,
    Label
} from 'reactstrap';
import Select from 'react-select';
import helper from '../services/helper';
class SimpleSelector extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: null,
            options: []
        }
    }
    async loadOptions() {
        let data = await helper.callPageApi(this.props.page, this.props.func);
        data.data.map(d => {
            d.value = d.id;
            d.label = d.name;
        });
        this.setState({ options: data.data });
        if (!this.state.selectedOption) {
            this.loadData(this.props.value)
        }
    }
    loadData(value) {
        for (var i = 0; i < this.state.options.length; i++) {
            if (this.state.options[i].value == value) {
                this.setState({ selectedOption: this.state.options[i] });
                break;
            }
        }
    }
    componentDidMount() {
        this.loadData(this.props.value);
        this.loadOptions();
    }
    componentWillReceiveProps(next) {
        this.loadData(next.value);
    }
    handleChange = (selectedOption) => {
        if (this.props.onChange) {
            this.props.onChange(selectedOption.value);
        }
        this.setState({ selectedOption });
    }
    render() {

        return (
            <Select
                value={this.state.selectedOption}
                onChange={this.handleChange}
                options={this.state.options}
            />
        );
    }
}

export default SimpleSelector;
