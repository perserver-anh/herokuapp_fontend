import React from 'react';
import Loadable from 'react-loadable'
import './index.css'
import DefaultLayout from './containers/DefaultLayout';

function Loading() {
    return <div> <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div><span>Hệ thống đang xử lý vui lòng đợi trong giây lát</span> </div>;
}



const Storis = Loadable({
    loader: () => import('./views/QuanLySanPham/Storis'),
    loading: Loading,
});


const Story = Loadable({
    loader: () => import('./views/QuanLySanPham/Storis/Story'),
    loading: Loading,
});

const Accounts = Loadable({
    loader: () => import('./views/QuanLyTaiKhoan/DanhSachNguoiDung'),
    loading: Loading,
});


const Account = Loadable({
    loader: () => import('./views/QuanLyTaiKhoan/DanhSachNguoiDung/Account'),
    loading: Loading,
});

const Categories = Loadable({
    loader: () => import('./views/QuanLySanPham/Category'),
    loading: Loading,
});
const Category = Loadable({
    loader: () => import('./views/QuanLySanPham/Category/Category'),
    loading: Loading,
});

const Dashboard = Loadable({
    loader: () => import('./views/Dashboard'),
    loading: Loading,
});

const Password = Loadable({
    loader: () => import('./views/Password/ChangePassword'),
    loading: Loading,
});
const ResetPassword = Loadable({
    loader: () => import('./views/Password/ResetPassword'),
    loading: Loading,
});


const Users = Loadable({
    loader: () => import('./views/Users/Users'),
    loading: Loading,
});


const User = Loadable({
    loader: () => import('./views/Users/User'),
    loading: Loading,
});

const PersonalInformation = Loadable({
    loader: () => import('./views/ThongTinNguoiDung/PersonalInformation'),
    loading: Loading,
});


// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
    { path: '/', exact: true, name: '', component: DefaultLayout },
    { path: '/dashboard', name: 'Trang chủ', component: Dashboard },
    { path: '/quanlysanpham/danhsach', exact: true, name: 'Danh sách truyện', component: Storis },
    { path: '/quanlysanpham/danhsach/:id', exact: true, name: 'Chi tiết', component: Story },

    { path: '/quanlytaikhoan/danhsach', exact: true, name: 'Danh sách người dùng', component: Accounts },
    { path: '/quanlytaikhoan/danhsach/:id', exact: true, name: 'Chi tiết', component: Account },
    { path: '/quanlysanpham/danhmuc', exact: true, name: 'Danh mục truyện tranh', component: Categories },
    { path: '/quanlysanpham/danhmuc/:id', exact: true, name: 'Chi tiết', component: Category },
    { path: '/password/changepassword', name: 'Mật khẩu', component: Password },
    { path: '/password/resetpassword', name: 'Reset mật khẩu', component: ResetPassword },

    { path: '/users', exact: true, name: 'Danh sách người dùng', component: Users },
    { path: '/users/:id', exact: true, name: 'Chi tiết', component: User },
    { path: '/user/profile', exact: true, name: 'Thông tin cá nhân', component: PersonalInformation },
];

export default routes;
